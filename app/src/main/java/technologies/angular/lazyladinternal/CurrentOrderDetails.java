package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.jpardogo.android.googleprogressbar.library.FoldingCirclesDrawable;
import com.melnykov.fab.FloatingActionButton;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CurrentOrderDetails extends ActionBarActivity {

    //private String m_servProvId;
    private int m_numberOfCurrentOrderItems;
    private String m_currentOrderCode;
    private String m_userCode;
    private Spinner m_DeliveryBoySpinner;
    private ArrayList<DeliveryBoysSnippet> m_nameList;
    private ArrayList<String> m_DeliveryNamesList;
    private String m_deliveryBoySelected;
    private Toolbar toolbar;

    private TextView m_to_be_paid;
    private TextView m_already_paid_online;

    private ArrayList<CurrentOrderDetailsSnippet> m_currentOrderDetailsList;
    private CurrentOrderDetailsAdapter m_currentOrderDetailsAdapter;
    private ListView m_currentOrderDetails;
    private FloatingActionButton m_confirmButton;

    private ArrayList<String> m_confirmedItemCodeDetailsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_order_details);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }
        if(!Utils.isNetworkAvailable(this)){
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        m_currentOrderDetails = (ListView)findViewById(R.id.current_order_details_list_view);
        m_confirmButton=(FloatingActionButton)findViewById(R.id.confirm_order_items_button);
        m_confirmButton.setVisibility(View.INVISIBLE);   //Set Visible in post execute of load order details

       // m_confirmButton.attachToListView(m_currentOrderDetails);

        Intent intent = getIntent();
        String data[]=intent.getStringArrayExtra("data_send");
        m_currentOrderCode=data[0];
        m_userCode=data[1];

        new LoadCurrentOrderDetails().execute(m_currentOrderCode);

        m_already_paid_online = (TextView)findViewById(R.id.already_paid_online);
        m_to_be_paid = (TextView)findViewById(R.id.to_be_paid);
        new LoadOrderDetails().execute(m_currentOrderCode,m_userCode);

        m_confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConifrmCurrentOrder();
            }
        });

    }

    private void ConifrmCurrentOrder(){
        m_confirmedItemCodeDetailsList=new ArrayList<String>();
        for(int i=0; i<m_currentOrderDetailsList.size(); i++){
            if(m_currentOrderDetailsList.get(i).m_itemConfirmed==1){
                String itemCode=m_currentOrderDetailsList.get(i).m_itemCode;
                Log.e("i",i+"");
                m_confirmedItemCodeDetailsList.add(itemCode);
            }
        }
        new ConfirmCurrentOrder().execute(m_currentOrderCode, m_userCode);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ConfirmCurrentOrder extends AsyncTask<String, String, String>{

        private static final String url_confirmCurrentOrderItemDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/confirmCurrentOrderItemDetailsForLazyLads";

        @Override
        protected String doInBackground(String... params) {

            JSONArray jsonObject = new JSONArray();
            for(int i=0; i< m_confirmedItemCodeDetailsList.size(); i++){
                JSONObject smallJsonObject = new JSONObject();
                String m_itemCode=m_confirmedItemCodeDetailsList.get(i);
                Log.e(" m_confirmed.size", m_confirmedItemCodeDetailsList.size()+"");
                try{
                    smallJsonObject.put("item_code", m_itemCode);
                    Log.e("item_code",m_itemCode);
                    jsonObject.put(smallJsonObject);
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            String response_confirmCurrentOrder;
            JSONObject json_data_confirmCurrentOrder;
            JSONArray confirmCurrentOrder_Array;
            HTTPConnectionHandler hTTPConnection_CheckOut = new HTTPConnectionHandler();

            String currentOrderCode=params[0];
            String userCode=params[1];

            ArrayList arrayList_confirmCurrentOrder = new ArrayList();
            Log.e("JsonDataArray",jsonObject.toString());
            arrayList_confirmCurrentOrder.add((Object)new BasicNameValuePair("JsonDataArray", jsonObject.toString()));
            arrayList_confirmCurrentOrder.add((Object)new BasicNameValuePair("current_order_code", currentOrderCode));
            arrayList_confirmCurrentOrder.add((Object)new BasicNameValuePair("user_code", userCode));


            try {
                Log.i("Non Available confirm", "Step2");
                response_confirmCurrentOrder = hTTPConnection_CheckOut.postResponse(url_confirmCurrentOrderItemDetails_serviceProvider, arrayList_confirmCurrentOrder);
                Log.i("Non Available Response", response_confirmCurrentOrder);
                if (response_confirmCurrentOrder == null) {
                    return null;
                } else {
                    json_data_confirmCurrentOrder = new JSONObject(response_confirmCurrentOrder);
                    try {
                        int success = json_data_confirmCurrentOrder.getInt("error");
                        if (success == 1) {
                            Log.i("successful", "its done");
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }
        protected void onPostExecute(String file_url) {
            onBackPressed();
            // updating UI from Background Thread
        }


    }

    class LoadCurrentOrderDetails extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private static final String url_currentOrderDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/currentOrdersDetailsForLazyLads";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CurrentOrderDetails.this);
            pDialog.setMessage("Loading Orders Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_currentOrderDetails_servProv;
            JSONObject json_data_currentOrderDetails_servProv;
            JSONArray currentOrderDetails_servProv;
            HTTPConnectionHandler hTTPConnection_currentOrderDetails_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_currentOrderDetails_ServProv = new ArrayList();

            String currentOrderCode=params[0];
            Log.i("current_order_code", currentOrderCode);
            arrayList_currentOrderDetails_ServProv.add((Object)new BasicNameValuePair("currentOrder_code", currentOrderCode));

            try{
                Log.i("In Asynch", "Step2");
                response_currentOrderDetails_servProv=hTTPConnection_currentOrderDetails_ServProv.postResponse(url_currentOrderDetails_serviceProvider, arrayList_currentOrderDetails_ServProv);
                Log.i("Response", response_currentOrderDetails_servProv);
                if(response_currentOrderDetails_servProv==null){
                    return null;
                }
                else{
                    json_data_currentOrderDetails_servProv=new JSONObject(response_currentOrderDetails_servProv);
                    try{
                        int success = json_data_currentOrderDetails_servProv.getInt("error");
                        if(success == 1){
                            currentOrderDetails_servProv=json_data_currentOrderDetails_servProv.getJSONArray("current_order_details_service_providers");
                            m_numberOfCurrentOrderItems=currentOrderDetails_servProv.length();
                            m_currentOrderDetailsList=new ArrayList<CurrentOrderDetailsSnippet>(m_numberOfCurrentOrderItems);

                            for(int i=0; i<m_numberOfCurrentOrderItems; i++){
                                JSONObject current_order_details_list_object = currentOrderDetails_servProv.getJSONObject(i);
                                int itemId=i+1;
                                String itemCode=current_order_details_list_object.getString("item_code");
                                String itemName=current_order_details_list_object.getString("item_name");
                                int itemImgFlag=current_order_details_list_object.getInt("item_img_flag");
                                String itemImgAdd=current_order_details_list_object.getString("item_img_address");
                                String itemShortDesc=current_order_details_list_object.getString("item_short_desc"); //Weight like 20 gm wali dabi
                                int itemQuantity=Integer.parseInt(current_order_details_list_object.getString("item_quantity"));
                                double itemCost=current_order_details_list_object.getDouble("item_cost");   //total cost i.e. cost*quantity
                                String itemDesc = current_order_details_list_object.getString("item_desc");

                                CurrentOrderDetailsSnippet currenOrderDetailObj = new CurrentOrderDetailsSnippet(itemId, itemCode, itemName, itemImgFlag, itemImgAdd,
                                        itemShortDesc, itemQuantity, itemCost, itemDesc);
                                m_currentOrderDetailsList.add(currenOrderDetailObj);
                            }

                        }else{

                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String file_url) {

            m_currentOrderDetailsAdapter = new CurrentOrderDetailsAdapter(CurrentOrderDetails.this, m_currentOrderDetailsList);
            m_currentOrderDetails.setAdapter((ListAdapter)m_currentOrderDetailsAdapter);
            m_confirmButton.setVisibility(View.VISIBLE);
            pDialog.dismiss();
            // updating UI from Background Thread
        }

    }

    class LoadOrderDetails extends AsyncTask<String, String, String[]> {

        private ProgressDialog pDialog;
        private static final String url_OrderDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/getOrderDetailsUser";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CurrentOrderDetails.this);
            pDialog.setMessage("Loading Orders Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String response_OrderDetails_servProv;
            JSONObject json_data_OrderDetails_servProv;
            JSONObject OrderPaymentDetails_servProv;
            HTTPConnectionHandler hTTPConnection_OrderDetails_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_OrderDetails_ServProv = new ArrayList();

            String OrderCode=params[0];
            String userCode = params[1];
            Log.i("order_code", OrderCode);
            arrayList_OrderDetails_ServProv.add((Object)new BasicNameValuePair("order_code", OrderCode));
            arrayList_OrderDetails_ServProv.add((Object)new BasicNameValuePair("user_code", userCode));

            try{
                Log.i("In Asynch", "Step2");
                response_OrderDetails_servProv=hTTPConnection_OrderDetails_ServProv.postResponse(url_OrderDetails_serviceProvider, arrayList_OrderDetails_ServProv);
                Log.i("Response", response_OrderDetails_servProv);
                if(response_OrderDetails_servProv==null){
                    return null;
                }
                else{
                    json_data_OrderDetails_servProv=new JSONObject(response_OrderDetails_servProv);
                    try{
                        int success = json_data_OrderDetails_servProv.getInt("error");
                        if(success == 1){
                            if(json_data_OrderDetails_servProv.has("order_payment_details")) {
                                OrderPaymentDetails_servProv = json_data_OrderDetails_servProv.getJSONObject("order_payment_details");

                                String to_be_paid = OrderPaymentDetails_servProv.getString("to_be_paid");
                                String paid = OrderPaymentDetails_servProv.getString("paid");

                                return (new String[]{to_be_paid, paid});
                            }


                        }else{

                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String... paymentDetails) {
            if(paymentDetails != null) {
                m_to_be_paid.setText(paymentDetails[0]);
                m_already_paid_online.setText(paymentDetails[1]);
            }
            pDialog.dismiss();
            // updating UI from Background Thread
        }

    }

}
