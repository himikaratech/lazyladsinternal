package technologies.angular.lazyladinternal;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * Created by user on 03-08-2015.
 */
public class CustomSuggestionContentProvider extends ContentProvider {


    public static final String AUTHORITY = "technologies.angular.lazyladinternal.provider";
    public static Uri CONTENT_URI_SQL;
    public static Uri CONTENT_URI_INSERT;
    public static Uri CONTENT_URI_UPDATE;
    public static Uri CONTENT_URI_SELECT;
    public static Uri CONTENT_URI_DELETE;
    public static Uri CONTENT_URI_UPDATENSELECT;

    private static UriMatcher uriMatcher;
    private DBHelper m_dbHelper;
    private SQLiteDatabase m_db;
    private Cursor m_cursor;

    static{
        CustomSuggestionContentProvider.CONTENT_URI_SQL = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SQL"));
        CustomSuggestionContentProvider.CONTENT_URI_INSERT = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/search_suggest_query"));
        CustomSuggestionContentProvider.uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SQL", 1);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SQL/#", 2);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "search_suggest_query", 3);
        CustomSuggestionContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "search_suggest_query/#", 4);

    }

    private static final String[] COLUMNS = {
            "_id", // must include this column
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_TEXT_2,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA};

    @Override
    public boolean onCreate() {
        m_dbHelper=new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = uri.getLastPathSegment();
        //  MatrixCursor cursor=null;
        if (query.length() > 2)
        {
            Log.i("ye to reach", "man");
            if (SearchManager.SUGGEST_URI_PATH_QUERY.equals(query)) {
                Log.i("reached", "but not fully");
                return null;
            } else {
                Log.i("reached man", "seriously reached");
                m_db = m_dbHelper.getReadableDatabase();
                Log.i("reached man2", query);

                // String sqlquery = "select * from search_result where search_string like '"+ Utils.strForSqlite(query)+"'  COLLATE NOCASE ";
                String sqlquery = "select * from search_result ";
                this.m_cursor = this.m_db.rawQuery(sqlquery, (String[]) (null));
                Log.i("reached man3", Integer.toString(this.m_cursor.getCount()));

                this.m_cursor.moveToFirst();
                Log.i("reached man4", "seriously reached");
                int n = 0;
                MatrixCursor cursor = new MatrixCursor(COLUMNS);
                for (int i = 0; i < this.m_cursor.getCount(); i++) {
                    Log.i("name", this.m_cursor.getString(9));
                    Log.i("code", this.m_cursor.getString(2));
                    cursor.addRow(createRow(new Integer(n), this.m_cursor.getString(9),this.m_cursor.getString(2) + " - " + this.m_cursor.getString(3), this.m_cursor.getString(2)));
                    n++;
                    this.m_cursor.moveToNext();
                }
                return cursor;
            }
        }
        return null;
    }


    private Object[] createRow(Integer id, String text1,String text2, String text3) {
        return new Object[] { id, // _id
                text1, text2, text3};
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
