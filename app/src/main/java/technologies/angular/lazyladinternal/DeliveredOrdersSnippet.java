package technologies.angular.lazyladinternal;

/**
 * Created by Saurabh on 30/01/15.
 */
public class DeliveredOrdersSnippet {

    public int deliveredOdersId;
    public String orderCode;
    public String address;
    public String timeOrderPlaced;
    public double amount;
    public String userName;
    public String userPhone;
    public String m_coupon;

    public DeliveredOrdersSnippet(){
    }

    public DeliveredOrdersSnippet(int id, String order_code, String customer_address, String time_order_placed, double total_amount, String user_name, String user_phone_num, String coupon){
        deliveredOdersId=id;
        orderCode=order_code;
        address=customer_address;
        timeOrderPlaced=time_order_placed;
        amount=total_amount;
        userName=user_name;
        userPhone=user_phone_num;
        m_coupon=coupon;
    }
}
