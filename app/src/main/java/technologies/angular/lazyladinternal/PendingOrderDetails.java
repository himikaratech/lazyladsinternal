package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class PendingOrderDetails extends ActionBarActivity {

    private int m_numberOfPendingOrderItems;
    private String m_pendingOrderCode;
    private String m_userCode;

    private TextView m_to_be_paid;
    private TextView m_already_paid_online;

    private ArrayList<PendingOrderDetailsSnippet> m_pendingOrderDetailsSnippet;
    private ListView m_pendingOrderListView;
    private PendingOrderDetailsAdapter m_pendingOrderDetailsAdapter;
    private FloatingActionButton m_confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_order_details);

        if(!Utils.isNetworkAvailable(this)){
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        m_pendingOrderListView=(ListView)findViewById(R.id.pending_order_details_listview);
        m_confirmButton=(FloatingActionButton)findViewById(R.id.confirm_pending_order_items_button);
        m_confirmButton.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        String data[]=intent.getStringArrayExtra("data_send_pending_details");
        m_pendingOrderCode=data[0];
        m_userCode=data[1];

        new LoadPendingOrderDetails().execute(m_pendingOrderCode);

        m_already_paid_online = (TextView)findViewById(R.id.already_paid_online);
        m_to_be_paid = (TextView)findViewById(R.id.to_be_paid);
        new LoadOrderDetails().execute(m_pendingOrderCode,m_userCode);

        m_confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConfirmPendingOrder().execute(m_pendingOrderCode, m_userCode);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pending_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ConfirmPendingOrder extends AsyncTask<String, String, String> {

        private static final String url_pendingOrders_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/confirmPendingOrdersItemDetailsForLazyLads";

        @Override
        protected String doInBackground(String... params) {
            String response_confirmPendingOrders_servProv;
            JSONObject json_data_confirmPendingOrders_servProv;
            HTTPConnectionHandler hTTPConnection_confirmPendingOrders_servProv = new HTTPConnectionHandler();
            ArrayList arrayList_confirmPendingOrders_servProv = new ArrayList();

            String pendingOrderCode=params[0];     //"1";
            String userCode=params[1];
            arrayList_confirmPendingOrders_servProv.add((Object)new BasicNameValuePair("pending_order_code", pendingOrderCode));
            arrayList_confirmPendingOrders_servProv.add((Object)new BasicNameValuePair("user_code", userCode));

            try{
                response_confirmPendingOrders_servProv=hTTPConnection_confirmPendingOrders_servProv.postResponse(url_pendingOrders_serviceProvider, arrayList_confirmPendingOrders_servProv);
                if(response_confirmPendingOrders_servProv==null){
                    return null;
                }
                else{
                    json_data_confirmPendingOrders_servProv=new JSONObject(response_confirmPendingOrders_servProv);
                    try{
                        int success = json_data_confirmPendingOrders_servProv.getInt("error");
                        if(success == 1){
                            //confirmPendingOrders_servProv=json_data_confirmPendingOrders_servProv.getJSONArray("confirm_pending_orders_service_providers");

                        }else{
                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            onBackPressed();
        }
    }

    class LoadPendingOrderDetails extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private static final String url_pendingOrderDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/pendingOrdersDetailsForLazyLads";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PendingOrderDetails.this);
            pDialog.setMessage("Loading Pending Order Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_pendingOrderDetails_servProv;
            JSONObject json_data_pendingOrderDetails_servProv;
            JSONArray pendingOrderDetails_servProv;
            HTTPConnectionHandler hTTPConnection_pendingOrderDetails_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_pendingOrderDetails_ServProv = new ArrayList();

            String pendingOrderCode=params[0];
            arrayList_pendingOrderDetails_ServProv.add((Object)new BasicNameValuePair("pendingOrder_code", pendingOrderCode));

            try{
                response_pendingOrderDetails_servProv=hTTPConnection_pendingOrderDetails_ServProv.postResponse(url_pendingOrderDetails_serviceProvider, arrayList_pendingOrderDetails_ServProv);
                if(response_pendingOrderDetails_servProv==null){
                    return null;
                }
                else{
                    json_data_pendingOrderDetails_servProv=new JSONObject(response_pendingOrderDetails_servProv);
                    try{
                        int success = json_data_pendingOrderDetails_servProv.getInt("error");
                        if(success == 1){
                            pendingOrderDetails_servProv=json_data_pendingOrderDetails_servProv.getJSONArray("pending_order_details_service_providers");
                            m_numberOfPendingOrderItems=pendingOrderDetails_servProv.length();
                            m_pendingOrderDetailsSnippet=new ArrayList<PendingOrderDetailsSnippet>(m_numberOfPendingOrderItems);

                            for(int i=0; i<m_numberOfPendingOrderItems; i++){
                                JSONObject pending_order_details_list_object = pendingOrderDetails_servProv.getJSONObject(i);
                                int itemId=i+1;
                                String itemCode=pending_order_details_list_object.getString("item_code");
                                String itemName=pending_order_details_list_object.getString("item_name");
                                int itemImgFlag=pending_order_details_list_object.getInt("item_img_flag");
                                String itemImgAdd=pending_order_details_list_object.getString("item_img_address");
                                String itemShortDesc=pending_order_details_list_object.getString("item_short_desc"); //Weight like 20 gm wali dabi
                                int itemQuantity=Integer.parseInt(pending_order_details_list_object.getString("item_quantity"));
                                double itemCost=pending_order_details_list_object.getDouble("item_cost");   //total cost i.e. cost*quantity
                                String itemDesc = pending_order_details_list_object.getString("item_desc");

                                PendingOrderDetailsSnippet pendOrderDetailObj = new PendingOrderDetailsSnippet(itemId, itemCode, itemName, itemImgFlag, itemImgAdd,
                                        itemShortDesc, itemQuantity, itemCost, itemDesc);
                                m_pendingOrderDetailsSnippet.add(pendOrderDetailObj);
                            }

                        }else{

                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String file_url) {

            m_pendingOrderDetailsAdapter = new PendingOrderDetailsAdapter(PendingOrderDetails.this, m_pendingOrderDetailsSnippet);
            m_pendingOrderListView.setAdapter((ListAdapter)m_pendingOrderDetailsAdapter);
            m_confirmButton.setVisibility(View.VISIBLE);
            pDialog.dismiss();
         }
    }

    class LoadOrderDetails extends AsyncTask<String, String, String[]> {

        private ProgressDialog pDialog;
        private static final String url_OrderDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/getOrderDetailsUser";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PendingOrderDetails.this);
            pDialog.setMessage("Loading Orders Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String response_OrderDetails_servProv;
            JSONObject json_data_OrderDetails_servProv;
            JSONObject OrderPaymentDetails_servProv;
            HTTPConnectionHandler hTTPConnection_OrderDetails_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_OrderDetails_ServProv = new ArrayList();

            String OrderCode=params[0];
            String userCode = params[1];
            Log.i("order_code", OrderCode);
            arrayList_OrderDetails_ServProv.add((Object)new BasicNameValuePair("order_code", OrderCode));
            arrayList_OrderDetails_ServProv.add((Object)new BasicNameValuePair("user_code", userCode));

            try{
                Log.i("In Asynch", "Step2");
                response_OrderDetails_servProv=hTTPConnection_OrderDetails_ServProv.postResponse(url_OrderDetails_serviceProvider, arrayList_OrderDetails_ServProv);
                Log.i("Response", response_OrderDetails_servProv);
                if(response_OrderDetails_servProv==null){
                    return null;
                }
                else{
                    json_data_OrderDetails_servProv=new JSONObject(response_OrderDetails_servProv);
                    try{
                        int success = json_data_OrderDetails_servProv.getInt("error");
                        if(success == 1){
                            if(json_data_OrderDetails_servProv.has("order_payment_details")) {
                                OrderPaymentDetails_servProv = json_data_OrderDetails_servProv.getJSONObject("order_payment_details");

                                String to_be_paid = OrderPaymentDetails_servProv.getString("to_be_paid");
                                String paid = OrderPaymentDetails_servProv.getString("paid");

                                return (new String[]{to_be_paid, paid});
                            }


                        }else{

                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String... paymentDetails) {
            if(paymentDetails != null) {
                m_to_be_paid.setText(paymentDetails[0]);
                m_already_paid_online.setText(paymentDetails[1]);
            }
            pDialog.dismiss();
            // updating UI from Background Thread
        }

    }
}
