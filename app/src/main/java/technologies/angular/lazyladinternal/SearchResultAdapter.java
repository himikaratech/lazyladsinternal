package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by user on 03-08-2015.
 */
public class SearchResultAdapter extends BaseAdapter {

    public Cursor m_cursor;
    public static ContentResolver m_conRes=null;
    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;
    private Activity m_activity;
    private ArrayList<SearchResultSnippet> m_searchResultItemsDetails;

    static{
        SearchResultAdapter.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    static{
        SearchResultAdapter.deleteUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
    }
    static{
        SearchResultAdapter.insertUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
    }
    static{
        SearchResultAdapter.updateUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
    }

    private class ViewHolder{
        Button cancel_order_button;
        TextView orderCancelled;
    }

    public SearchResultAdapter(Activity activity, ArrayList<SearchResultSnippet> searchResultItemDetails) {
        m_activity = activity;
        m_searchResultItemsDetails = searchResultItemDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_searchResultItemsDetails != null) {
            count = m_searchResultItemsDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_searchResultItemsDetails != null) {
            return m_searchResultItemsDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_searchResultItemsDetails != null) {
            return m_searchResultItemsDetails.get(position).ordersId;
        }
        return 0;
    }

    public static String GetHumanReadableDate(long epochSec, String dateFormatStr) {
        Date date = new Date(epochSec * 1000);
        SimpleDateFormat format = new SimpleDateFormat(dateFormatStr);
        return format.format(date);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (m_searchResultItemsDetails != null) {
            final ViewHolder holder;

            final int positionFinal = position;
                final String orderCode=((SearchResultSnippet) getItem(position)).m_orderCode;
            String orderAddress=((SearchResultSnippet) getItem(position)).m_address;
            int orderAmount=((SearchResultSnippet) getItem(position)).m_amount;
            String orderTimePlaced=((SearchResultSnippet) getItem(position)).m_timeOrderPlaced;
            Long engTime = ((SearchResultSnippet) getItem(position)).m_expectedTime;
            String orderTimeExpected = GetHumanReadableDate(engTime, "E LLL dd, yyyy HH:mm");
            String orderUserName = ((SearchResultSnippet)getItem(position)).m_userName;
            String orderPhoneNumber = ((SearchResultSnippet)getItem(position)).m_userPhoneNum;
            final String userCode=((SearchResultSnippet) getItem(position)).m_userCode;
            String orderStatus = ((SearchResultSnippet) getItem(position)).m_orderStatus;
            Log.i("orderStaus", orderStatus);
            String orderShopName = ((SearchResultSnippet) getItem(position)).m_spName;
            Long sellerDeliveryTime = ((SearchResultSnippet) getItem(position)).m_sellerTime;
            //String ordersellerDelTime = GetHumanReadableDate(sellerDeliveryTime, "E LLL dd, yyyy HH:mm");

            if(convertView == null){
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.search_result_display_list, (ViewGroup)null);
                holder.cancel_order_button = (Button) convertView.findViewById(R.id.cancel_prev_order_button);
                holder.orderCancelled = (TextView) convertView.findViewById(R.id.order_cancelled);
                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder)convertView.getTag();
                convertView.setOnClickListener(null);
            }
            TextView searchUserCodeTextView=(TextView)convertView.findViewById(R.id.search_user_code_textView);
            TextView searchOrderCodeTextView=(TextView)convertView.findViewById(R.id.search_order_code_textview);
            TextView searchOrderAddressTextView=(TextView)convertView.findViewById(R.id.search_order_address_textview);
            TextView searchOrderAmountTextView=(TextView)convertView.findViewById(R.id.amount_search_order_textview);
            TextView searchOrderTimePlacedTextView=(TextView)convertView.findViewById(R.id.search_order_del_textview);
            TextView searchOrderTimeExpectedTextView=(TextView)convertView.findViewById(R.id.search_order_exp_textview);
            TextView searchOrderUserNameTextView =(TextView)convertView.findViewById(R.id.search_order_username_textview);
            TextView searchOrderUserPhoneTextView =(TextView)convertView.findViewById(R.id.search_order_number_textview);
            TextView searchOrderShopNameTextView =(TextView)convertView.findViewById(R.id.search_order_shopname_textview);
            TextView searchOrderSellerDelTimeTextView =(TextView)convertView.findViewById(R.id.search_seller_del_time);
            final TextView searchOrderStatusTextView =(TextView)convertView.findViewById(R.id.search_order_status_textView);

            LinearLayout header =(LinearLayout)convertView.findViewById(R.id.head_order);
            holder.orderCancelled.setText("");
            holder.cancel_order_button.setVisibility(View.VISIBLE);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            header.setBackgroundColor(color);

            searchUserCodeTextView.setText(userCode);
            searchOrderCodeTextView.setText(orderCode);
            searchOrderAddressTextView.setText(orderAddress);
            searchOrderAmountTextView.setText("₹ " + Integer.toString(orderAmount));
            searchOrderTimePlacedTextView.setText(orderTimePlaced);
            searchOrderTimeExpectedTextView.setText(orderTimeExpected);
            searchOrderUserNameTextView.setText(orderUserName);
            searchOrderUserPhoneTextView.setText(orderPhoneNumber);
            searchOrderShopNameTextView.setText(orderShopName);
            searchOrderSellerDelTimeTextView.setText(sellerDeliveryTime/(60.0) + "mins");
            searchOrderStatusTextView.setText(orderStatus);

            if (orderStatus.equals("Cancelled")) {
                holder.orderCancelled.setText("Cancelled");
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if((((SearchResultSnippet) getItem(positionFinal)).m_orderStatus).equals("Pending"))
                        startCurrentOrderDetilsActivity(orderCode, userCode);

                    else if((((SearchResultSnippet) getItem(positionFinal)).m_orderStatus).equals("Confirmed"))
                        startPendingOrderDetilsActivity(orderCode, userCode);

                   }
            });

            holder.cancel_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelThisOrder(orderCode);
                    ((SearchResultSnippet) getItem(positionFinal)).m_orderStatus = "Cancelled";
                    holder.orderCancelled.setText("Cancelled");
                    holder.cancel_order_button.setVisibility(View.INVISIBLE);
                }
            });
        }
        return convertView;
    }

    private void startCurrentOrderDetilsActivity(String currentOrderCode, String userCode){
        Intent intent = new Intent(m_activity, CurrentOrderDetails.class);
        String data[] = {currentOrderCode, userCode};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }

    private void startPendingOrderDetilsActivity(String pendingOrderCode, String userCode){
        Intent intent = new Intent(m_activity, PendingOrderDetails.class);
        String data[] = {pendingOrderCode, userCode};
        intent.putExtra("data_send_pending_details", data);
        m_activity.startActivity(intent);
    }

    private void cancelThisOrder(final String order_code){
        String url_cancel_order = "http://www.angulartechnologies.com/task_manager/v1/cancelUsersPreviousOrder";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_code", order_code);

        JsonObjectRequest sendOrderDetails = new JsonObjectRequest(url_cancel_order, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("error");
                            if (success == 1) {

                             /*   SearchResultAdapter.m_conRes.query(SearchResultAdapter.updateUri, SearchResultAdapter.dummy, "update" +
                                                " search_result set order_status =" + "Cancelled" + " where order_code ='" + order_code + "' ",
                                        SearchResultAdapter.dummy, "");
*/
                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(m_activity);
        m_sessionManager.addToRequestQueue(sendOrderDetails);
    }
}
