package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.jpardogo.android.googleprogressbar.library.FoldingCirclesDrawable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 06-08-2015.
 */
public class CitySelection extends Fragment {

    private ListView m_cities;
    private ArrayList<CitySnippet> m_cityList = null;
    private SessionManager m_sessionManager;
    private CityAdapter m_cityAdapter;
    private JSONObject jObject = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.city_selection, container, false);
        m_sessionManager = new SessionManager(getActivity());


        if (jObject == null){
            SharedPreferences sprefs = getActivity().getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
            try {
                jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(Utils.isNetworkAvailable(getActivity())){
            m_cities = (ListView)v.findViewById(R.id.cities);
           if(m_cityList == null) {
               loadCities();
           }
            else{
                m_cities.setAdapter((ListAdapter) m_cityAdapter);
              }
            }
        else {
        Toast.makeText(getActivity(), "Check your internet connectivity", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

    private void loadCities(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminateDrawable(new FoldingCirclesDrawable.Builder(getActivity()).build());
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_bar_city);



        String url_items_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/getCities";
        final GsonRequest<CityModel> jsObjRequest = new GsonRequest<CityModel>(Request.Method.GET,
                url_items_serviceProvider, CityModel.class, null, new Response.Listener<CityModel>() {
            @Override
            public void onResponse(CityModel response) {
                m_cityList=response.cities;
                m_cityAdapter = new CityAdapter(getActivity(), m_cityList, jObject);
                m_cities.setAdapter((ListAdapter) m_cityAdapter);

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error even: ", error.getMessage());
                progressDialog.dismiss();
            }
        });
        m_sessionManager.addToRequestQueue(jsObjRequest);
    }

    public JSONObject getData() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        Boolean selected;
        String checkedItems = "";
        CityAdapter adapter = (CityAdapter) m_cities.getAdapter();
        int totalChecked = adapter.cblist.size();
        for (int i = 0; i < totalChecked; i++) {
            selected = adapter.cblist.get(i);
            if (selected) {
                checkedItems = ((CitySnippet) (adapter.getItem(i))).m_cityCode;
                Log.i("Checked Item", checkedItems);
                jsonArray.put(checkedItems);
            }
            else if(!selected){
                checkedItems = "";
            }
        }
        try{
            jsonObject.put("cities", jsonArray);
        }catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("JSon object Values", jsonObject+"");
        return jsonObject;
    }
}
