package technologies.angular.lazyladinternal;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by user on 03-08-2015.
 */
public interface SearchApi {

    public class SearchModel {
        public int error;

        public ArrayList<SearchResultSnippet> orders_service_providers;

    }

    public class InputForSearchOrders {

        @SerializedName("search_string")
        public String searchString;

        @SerializedName("previous_order_code")
        public String beforeCode;

        @SerializedName("order_filter")
        public String orderFilter;

        public InputForSearchOrders(String search, String beforeOrderCode, String orderFilterData) {
            this.searchString = search;
            this.beforeCode = beforeOrderCode;
            this.orderFilter=orderFilterData;
        }
    }

    @POST("/searchOrdersForLazyLadsLimited")
    void searchFeeds(@Body InputForSearchOrders searchOrders, Callback<SearchModel> callback);
}
