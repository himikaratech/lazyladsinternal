package technologies.angular.lazyladinternal;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public final class CommonUtilities {

    static final String DISPLAY_MESSAGE_ACTION =
            "com.example.physics.simplified.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
    	Log.i("In common", "hello");
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        Log.i("In common", "hello");
        intent.putExtra(EXTRA_MESSAGE, message);
        Log.i("In common", "hello");
        context.sendBroadcast(intent);
        Log.i("In common", "hello");
    }
}
