package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.melnykov.fab.FloatingActionButton;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 16/11/15.
 */
public class AssignOrChangeDeliveryBoy extends ActionBarActivity {

    private Spinner m_DeliveryBoySpinner;
    private ArrayList<DeliveryBoysSnippet> m_nameList;
    private ArrayList<String> m_DeliveryNamesList;
    private String m_deliveryBoyNameSelected;
    private String m_deliveryBoyIdSelected;
    private TextView deliveryBoyName;
    private String order_code;
    private int riderFlag;

    private FloatingActionButton selectedDeliveryBoy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assign_change_del_boy);

        Intent intent = getIntent();
        order_code = intent.getStringExtra("order_code");
        riderFlag = intent.getIntExtra("riderFlag", 0);

        m_DeliveryBoySpinner = (Spinner) findViewById(R.id.delivery_boy_spinner);
        deliveryBoyName = (TextView) findViewById(R.id.delivery_boy_name);
        selectedDeliveryBoy = (FloatingActionButton) findViewById(R.id.confirm_lazystar_button);

        if (riderFlag == 1) {
            m_DeliveryBoySpinner.setVisibility(View.INVISIBLE);
            selectedDeliveryBoy.setVisibility(View.INVISIBLE);
            deliveryBoyName.setVisibility(View.VISIBLE);
        } else {
            m_DeliveryBoySpinner.setVisibility(View.VISIBLE);
            selectedDeliveryBoy.setVisibility(View.VISIBLE);
            deliveryBoyName.setVisibility(View.VISIBLE);
        }
        loadDeliveryBoyNames();

        m_DeliveryBoySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                startImplementation(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        selectedDeliveryBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new submitLazyStar().execute();
            }
        });
    }

    private void startImplementation(int position) {
        //  if(position!= (m_DeliveryNamesList.size()-1) ) {
        m_deliveryBoyNameSelected = m_nameList.get(position).m_BoyName;
        m_deliveryBoyIdSelected = m_nameList.get(position).m_BoyId;
        deliveryBoyName.setText(m_deliveryBoyNameSelected);
     /*   }
        else if(position == (m_DeliveryNamesList.size()-1) && m_DeliveryBoySpinner.getSelectedItem().toString().equals("Show More")){
            loadAllStars();
        }*/
    }


    private void loadDeliveryBoyNames() {

        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(this);

        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(AssignOrChangeDeliveryBoy.this);
        pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        String url_riders_serviceProvider = "http://www.angulartechnologies.com/newserver/logistics/get_riders_list";
        String param = order_code;
        url_riders_serviceProvider = url_riders_serviceProvider + "?order_code=" + param;

        GsonRequest<technologies.angular.lazyladinternal.DeliveryBoysModel> jsObjRequest = new GsonRequest<DeliveryBoysModel>(Request.Method.GET,
                url_riders_serviceProvider, technologies.angular.lazyladinternal.DeliveryBoysModel.class, null, new com.android.volley.Response.Listener<DeliveryBoysModel>() {
            @Override
            public void onResponse(technologies.angular.lazyladinternal.DeliveryBoysModel response) {
                Log.i("Success", response.success+"");
                if(false == response.success){
                    m_DeliveryBoySpinner.setVisibility(View.INVISIBLE);
                    selectedDeliveryBoy.setVisibility(View.INVISIBLE);
                    deliveryBoyName.setVisibility(View.INVISIBLE);
                    Toast.makeText(AssignOrChangeDeliveryBoy.this, "Some error occurred on server", Toast.LENGTH_SHORT).show();
                }
                if (true == response.success) {
                    String assignedLazyStar = null;
                    m_nameList = new ArrayList<technologies.angular.lazyladinternal.DeliveryBoysSnippet>();
                    String cityCode = response.city_code;
                    if (response.rider_assigned != null) {
                        assignedLazyStar = response.rider_assigned.rider_name;
                        deliveryBoyName.setText(assignedLazyStar);
                    } else
                        deliveryBoyName.setText("Not Yet Assigned");

                    if (riderFlag == 0) {
                        m_nameList = response.riders_available.get(cityCode);
              /*  for (int i = 0; i < response.riders_availabe.size(); i++) {
                    String city_code = response.riders_availabe.get(i).city_code;
                    if (city_code.equals(cityCode)){
                        m_nameList = response.riders_availabe.get(i).riders;
                        break;
                    }
                }
*/
                        m_DeliveryNamesList = new ArrayList<String>(m_nameList.size());
                        for (int i = 0; i < m_nameList.size(); i++) {
                            m_DeliveryNamesList.add(m_nameList.get(i).m_BoyName);
                        }

                        //  m_DeliveryNamesList.add("Show More");
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_boy_names, m_DeliveryNamesList);
                        dataAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown);
                        m_DeliveryBoySpinner.setAdapter(dataAdapter);
                        deliveryBoyName.setText(assignedLazyStar);
                    }
                }
                else {
                    m_DeliveryBoySpinner.setVisibility(View.INVISIBLE);
                    selectedDeliveryBoy.setVisibility(View.INVISIBLE);
                    deliveryBoyName.setVisibility(View.INVISIBLE);
                    Toast.makeText(AssignOrChangeDeliveryBoy.this, "Some error occurred on server", Toast.LENGTH_SHORT).show();
                }
                pDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error", error.getMessage());
                pDialog.dismiss();
            }
        });
        m_sessionManager.addToRequestQueue(jsObjRequest);
    }

    /* private void loadAllStars() {

         SessionManager m_sessionManager;
         m_sessionManager = new SessionManager(this);

         final ProgressDialog pDialog;

         pDialog = new ProgressDialog(AssignOrChangeDeliveryBoy.this);
         pDialog.setMessage("Please wait...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();

         String url_items_serviceProvider = "http://www.angulartechnologies.com/newserver/logistics/get_riders_list";
         String param = order_code;
         url_items_serviceProvider=url_items_serviceProvider + "/" + param;

         GsonRequest<technologies.angular.lazyladinternal.DeliveryBoysModel> jsObjRequest = new GsonRequest<DeliveryBoysModel>(Request.Method.GET,
                 url_items_serviceProvider, technologies.angular.lazyladinternal.DeliveryBoysModel.class, null, new com.android.volley.Response.Listener<DeliveryBoysModel>() {
             @Override
             public void onResponse(technologies.angular.lazyladinternal.DeliveryBoysModel response) {

                 m_nameList = new ArrayList<technologies.angular.lazyladinternal.DeliveryBoysSnippet>();
                 String cityCode = response.city_code;
                 String assignedLazyStar = response.rider_assigned;
                 if(assignedLazyStar != null)
                     deliveryBoyName.setText(assignedLazyStar);
                 else
                     deliveryBoyName.setText("Not Yet Assigned");
                 for (int i = 0; i < response.riders_availabe.size(); i++) {
                        m_nameList = response.riders_availabe.get(i).riders;
                 }

                 m_DeliveryNamesList = new ArrayList<String>(m_nameList.size());
                 for (int i = 0; i < m_nameList.size(); i++) {
                     m_DeliveryNamesList.add(m_nameList.get(i).m_BoyName);
                 }
                 ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_boy_names, m_DeliveryNamesList);
                 dataAdapter.setDropDownViewResource(R.layout.my_spinner_dropdown);
                 m_DeliveryBoySpinner.setAdapter(dataAdapter);
                 pDialog.dismiss();
             }
         }, new com.android.volley.Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 VolleyLog.e("Error even: ", error.getMessage());
                 pDialog.dismiss();
             }
         });
         m_sessionManager.addToRequestQueue(jsObjRequest);
     }
 */
    class submitLazyStar extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private static final String url_selectedRider_serviceProvider = "http://www.angulartechnologies.com/newserver/logistics/assign_rider";
        ;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AssignOrChangeDeliveryBoy.this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_selectedRider_servProv;
            JSONObject json_data_selectedRider_servProv;
            HTTPConnectionHandler hTTPConnection_selectedRider_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_selectedRider_ServProv = new ArrayList();

            arrayList_selectedRider_ServProv.add((Object) new BasicNameValuePair("rider_uid", m_deliveryBoyIdSelected));
            arrayList_selectedRider_ServProv.add((Object) new BasicNameValuePair("order_code", order_code));

            try {
                response_selectedRider_servProv = hTTPConnection_selectedRider_ServProv.postResponse(url_selectedRider_serviceProvider, arrayList_selectedRider_ServProv);
                if (response_selectedRider_servProv == null) {
                    return null;
                } else {
                    json_data_selectedRider_servProv = new JSONObject(response_selectedRider_servProv);
                    boolean success = json_data_selectedRider_servProv.getBoolean("success");
                    if (success == true) {
                    } else {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s) {
            pDialog.dismiss();
            onBackPressed();
        }
    }
}
