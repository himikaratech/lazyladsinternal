package technologies.angular.lazyladinternal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 12-08-2015.
 */
public class DeliveryBoysSnippet {

    @SerializedName("rider_phone")
    public String m_BoyId;
    @SerializedName("rider_name")
    public String m_BoyName;

    public DeliveryBoysSnippet(){
    }

    public DeliveryBoysSnippet(String id, String boyName){
        m_BoyId=id;
        m_BoyName=boyName;
    }
}
