package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DeliveredOrderDetails extends ActionBarActivity {

    private int m_numberOfDeliveredOrderItems;
    private String m_deliveredOrderCode;

    private ArrayList<DeliveredOrderDetailsSnippet> m_deliveredOrderDetailsSnippet;
    private ListView m_deliveredOrderListView;
    private DeliveredOrderDetailsAdapter m_deliveredOrderDetailsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivered_order_details);

        if(!Utils.isNetworkAvailable(this)){
            Toast.makeText(this, "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }

        m_deliveredOrderListView=(ListView)findViewById(R.id.delivered_order_details_listview);

        Intent intent = getIntent();
        String data[]=intent.getStringArrayExtra("data_send_delivered_details");
        m_deliveredOrderCode=data[0];

        new LoadDeliveredOrderDetails().execute(m_deliveredOrderCode);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delivered_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class LoadDeliveredOrderDetails extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private static final String url_deliveredOrderDetails_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/pendingOrdersDetailsForLazyLads";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DeliveredOrderDetails.this);
            pDialog.setMessage("Loading Delivered Order Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_deliveredOrderDetails_servProv;
            JSONObject json_data_deliveredOrderDetails_servProv;
            JSONArray deliveredOrderDetails_servProv;
            HTTPConnectionHandler hTTPConnection_deliveredOrderDetails_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_deliveredOrderDetails_ServProv = new ArrayList();

            String deliveredOrderCode=params[0];
            arrayList_deliveredOrderDetails_ServProv.add((Object)new BasicNameValuePair("pendingOrder_code", deliveredOrderCode));

            try{
                response_deliveredOrderDetails_servProv=hTTPConnection_deliveredOrderDetails_ServProv.postResponse(url_deliveredOrderDetails_serviceProvider, arrayList_deliveredOrderDetails_ServProv);
                if(response_deliveredOrderDetails_servProv==null){
                    return null;
                }
                else{
                    json_data_deliveredOrderDetails_servProv=new JSONObject(response_deliveredOrderDetails_servProv);
                    try{
                        int success = json_data_deliveredOrderDetails_servProv.getInt("error");
                        if(success == 1){
                            deliveredOrderDetails_servProv=json_data_deliveredOrderDetails_servProv.getJSONArray("pending_order_details_service_providers");
                            m_numberOfDeliveredOrderItems=deliveredOrderDetails_servProv.length();
                            m_deliveredOrderDetailsSnippet=new ArrayList<DeliveredOrderDetailsSnippet>(m_numberOfDeliveredOrderItems);

                            for(int i=0; i<m_numberOfDeliveredOrderItems; i++){
                                JSONObject delivered_order_details_list_object = deliveredOrderDetails_servProv.getJSONObject(i);
                                int itemId=i+1;
                                String itemCode=delivered_order_details_list_object.getString("item_code");
                                String itemName=delivered_order_details_list_object.getString("item_name");
                                int itemImgFlag=delivered_order_details_list_object.getInt("item_img_flag");
                                String itemImgAdd=delivered_order_details_list_object.getString("item_img_address");
                                String itemShortDesc=delivered_order_details_list_object.getString("item_short_desc"); //Weight like 20 gm wali dabi
                                int itemQuantity=Integer.parseInt(delivered_order_details_list_object.getString("item_quantity"));
                                double itemCost=delivered_order_details_list_object.getDouble("item_cost");   //total cost i.e. cost*quantity
                                String itemDesc = delivered_order_details_list_object.getString("item_desc");

                                DeliveredOrderDetailsSnippet pendOrderDetailObj = new DeliveredOrderDetailsSnippet(itemId, itemCode, itemName, itemImgFlag, itemImgAdd,
                                        itemShortDesc, itemQuantity, itemCost, itemDesc);
                                m_deliveredOrderDetailsSnippet.add(pendOrderDetailObj);
                            }

                        }else{

                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){

            }
            return null;
        }

        protected void onPostExecute(String file_url) {

            m_deliveredOrderDetailsAdapter = new DeliveredOrderDetailsAdapter(DeliveredOrderDetails.this, m_deliveredOrderDetailsSnippet);
            m_deliveredOrderListView.setAdapter((ListAdapter)m_deliveredOrderDetailsAdapter);
            pDialog.dismiss();
        }
    }
}
