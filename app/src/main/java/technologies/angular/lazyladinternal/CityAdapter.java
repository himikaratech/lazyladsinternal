package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by user on 06-08-2015.
 */
public class CityAdapter extends BaseAdapter {

    private Activity m_activity;
    private ArrayList<CitySnippet> m_cityList;
    public ArrayList<Boolean> cblist;
    private JSONObject m_jObject;
    JSONArray values = new JSONArray();

    public CityAdapter(Activity activity, ArrayList<CitySnippet> cityList, JSONObject jObject) {
        m_activity = activity;
        m_cityList = cityList;
        m_jObject = jObject;
        try {
            values = m_jObject.getJSONArray("cities");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cblist = new ArrayList<>(Collections.nCopies(m_cityList.size(), false));
        for(int i=0; i < m_cityList.size(); i++) {
           for(int j=0; j < values.length(); j++) {
               try {
                   if (m_cityList.get(i).m_cityCode.equals(values.getString(j))) {
                       cblist.set(i, true);
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
        }
    }

    private class ViewHolder{
        TextView cityNameView;
        CheckBox cityCB;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_cityList != null){
            count=m_cityList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_cityList != null){
            return m_cityList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_cityList != null){
            return m_cityList.get(position).m_cityId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(m_cityList != null) {
            ViewHolder vh;
            if (null == convertView) {
                vh = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.ciy_details_list, parent, false);

                vh.cityNameView = (TextView) (convertView.findViewById(R.id.city_name));
                vh.cityCB = (CheckBox) (convertView.findViewById(R.id.city_checkbox));
                convertView.setTag(vh);

            } else {
                vh = (ViewHolder) convertView.getTag();
                vh.cityCB.setOnCheckedChangeListener(null);
            }

            if(cblist.get(position))
                vh.cityCB.setChecked(true);
            else
                vh.cityCB.setChecked(false);

            vh.cityNameView.setText(m_cityList.get(position).m_cityName);
            vh.cityCB.setTag(position);
            vh.cityCB.setChecked(cblist.get(position));
            vh.cityCB.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            // Get the position that set for the checkbox using setTag.
                            int getPosition = (Integer) buttonView.getTag();
                            // Set the value of checkbox to maintain its state.
                            cblist.set(getPosition, buttonView.isChecked());
                        }
                    });
          //  vh.cityCB.setChecked(cblist.get(position));
                       return convertView;
        }
        return null;
    }
}
