package technologies.angular.lazyladinternal;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    private Button m_currentOrdersButton;
    private Button m_pendingOrdersButton;
    private Button m_deliveredOrdersButton;
    private MenuItem searchMenuItem;
    String API ="http://www.angulartechnologies.com/task_manager/v1";
    Toolbar toolbar;

    private AsyncTask m_async5;
    public static ContentResolver m_conRes=null;
    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;

    static{
        MainActivity.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    static{
        MainActivity.deleteUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
    }
    static{
        MainActivity.insertUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
    }
    static{
        MainActivity.updateUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setTitle("Home");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        m_conRes = this.getContentResolver();
        m_currentOrdersButton=(Button)findViewById(R.id.current_orders_button);
        m_pendingOrdersButton=(Button)findViewById(R.id.pending_orders_button);
        m_deliveredOrdersButton=(Button)findViewById(R.id.delivered_orders_button);

        m_currentOrdersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCurrentOrdersActivity();
            }
        });

        m_pendingOrdersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPendingOrdersActivity();
            }
        });

        m_deliveredOrdersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDeliveredOrdersActivity();
            }
        });

    }

    private void startCurrentOrdersActivity(){
        Intent intent = new Intent(this, TabViewForOrders.class);
        startActivity(intent);
    }

    private void startPendingOrdersActivity(){
        Intent intent = new Intent(this, PendingOrders.class);
        startActivity(intent);
    }

    private void startDeliveredOrdersActivity(){
        Intent intent = new Intent(this, DeliveredOrders.class);
        startActivity(intent);
    }


    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MenuItem searchMenuItem = getSearchMenuItem();
                if (searchMenuItem != null) {
                    SearchView sv = (SearchView) searchMenuItem.getActionView();
                    sv.onActionViewCollapsed();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("Delete db", "Delete");
                MainActivity.m_conRes.query(MainActivity.deleteUri, MainActivity.dummy, "delete from search_result", MainActivity.dummy, "");
                if (s.length() > 2) {
                    Log.i("location", "TextChange");
                    loadData(s);
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadData(final String searchText) {
        JSONObject jObject = null;
        //   String sendToServer[];
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        SearchApi searchService = restAdapter.create(SearchApi.class);
        Log.i("search_text", searchText);
        SharedPreferences sprefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
        try {
            jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Jobject_search_order",jObject.toString());
        SearchApi.InputForSearchOrders search = new SearchApi.InputForSearchOrders(searchText, "0", jObject.toString());
        searchService.searchFeeds(search, new Callback<SearchApi.SearchModel>() {
            @Override
            public void success(SearchApi.SearchModel orderResults, Response response) {
                int success = orderResults.error;
                if (success == 1) {

                    ArrayList<SearchResultSnippet> orderList = orderResults.orders_service_providers;

                    Log.e("sucess tabViewForOrders", orderResults.error + "");
                    m_async5 = new StoreItemsInSqllite().execute(orderList);

                } else {
                    Toast.makeText(getApplicationContext(), "No result Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
            }
        });
    }

    private class StoreItemsInSqllite extends AsyncTask<ArrayList, Void, Void> {
        @Override
        protected Void doInBackground(ArrayList... params) {
            ArrayList<SearchResultSnippet> orderList = params[0];
            if(orderList == null)
                return null;

            for (int i = 0; i < orderList.size(); i++) {
                if (isCancelled()) {
                    break;
                }

                Log.i("search_result ", "working");

                int ordersId = i+1;
                String orderCode = orderList.get(i).m_orderCode;
                String address = Utils.strForSqlite(orderList.get(i).m_address);
                String userCode = orderList.get(i).m_userCode;
                String timeOrderPlaced = orderList.get(i).m_timeOrderPlaced;
                Long expectedTime = orderList.get(i).m_expectedTime;
                int amount = orderList.get(i).m_amount;
                String userName = Utils.strForSqlite(orderList.get(i).m_userName);
                String userPhoneNum = orderList.get(i).m_userPhoneNum;
                String orderStatus = orderList.get(i).m_orderStatus;
                String spName = Utils.strForSqlite(orderList.get(i).m_spName);
                Long sellerDelTime = orderList.get(i).m_sellerTime;

                Log.i("data for order status", orderStatus);
                m_conRes.query(TabViewForOrders.insertUri, dummy, "insert into search_result (order_id, order_code, order_address, " +
                        "user_code, time_order_placed, expected_time, amount, user_name, user_phone, order_status, " +
                        "sp_name, seller_time) values ('" + ordersId + "', '" + orderCode + "', '" + address + "', '" + userCode + "', " +
                        "'" + timeOrderPlaced + "', '" + expectedTime + "', '" + amount + "', '" + userName + "', '" + userPhoneNum + "', " +
                        "'" + orderStatus + "', '" + spName + "', '" + sellerDelTime + "')", dummy, "");
            }
            return null;
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences sp = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.clear().commit();
    }
}
