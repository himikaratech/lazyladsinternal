package technologies.angular.lazyladinternal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 06-08-2015.
 */
public class CitySnippet {

    @SerializedName("city_id")
    public int m_cityId;
    @SerializedName("city_code")
    public String m_cityCode;
    @SerializedName("city_name")
    public String m_cityName;

    public CitySnippet(){
    }

    public CitySnippet(int id, String cityCode, String cityName){
        m_cityId=id;
        m_cityCode=cityCode;
        m_cityName=cityName;
    }

}
