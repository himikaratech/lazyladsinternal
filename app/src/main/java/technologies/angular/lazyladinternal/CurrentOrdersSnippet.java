package technologies.angular.lazyladinternal;

import org.json.JSONObject;

/**
 * Created by Saurabh on 29/01/15.
 */
public class CurrentOrdersSnippet {

    public int currentOrdersId;
    public String m_currentOrderCode;
    public String m_address;
    public String m_userCode;
    public String m_timeOrderPlaced;
    public int m_amount;
    public String m_userName;
    public String m_userPhoneNum;
    public String m_orderStatus;
    public String m_spName;
    public String m_coupon;
    public int m_delBoyRequested;
    public int m_delBoyAssigned;
    public int m_delBoyConfirmed;
    public String m_verifiedNumber;

    public CurrentOrdersSnippet(){
    }

    public CurrentOrdersSnippet(int id, String currentOrderCode, String customer_address, String user_code, String time_order_placed, int total_amount, String userName, String userPhoneNum, String spName, String coupon, String verified_number, int delBoyRequested, int delBoyAssigned, int delBoyConfirmed) {
        currentOrdersId = id;
        m_currentOrderCode = currentOrderCode;
        m_address = customer_address;
        m_userCode = user_code;
        m_timeOrderPlaced = time_order_placed;
        m_amount = total_amount;
        m_userName = userName;
        m_userPhoneNum = userPhoneNum;
        m_orderStatus = "Pending";
        m_spName = spName;
        m_coupon = coupon;
        m_delBoyRequested = delBoyRequested;
        m_delBoyAssigned = delBoyAssigned;
        m_delBoyConfirmed = delBoyConfirmed;

        m_verifiedNumber = verified_number;
    }
}
