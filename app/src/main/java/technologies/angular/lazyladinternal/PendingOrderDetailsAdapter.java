package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 16/03/15.
 */
public class PendingOrderDetailsAdapter extends BaseAdapter {

    private ArrayList<PendingOrderDetailsSnippet> m_pendingOrderDetails;
    private Activity m_activity;

    private class ViewHolder {
        //TextView itemCodeTextView=(TextView)convertView.findViewById(R.id.item_code_textview);
        TextView itemNameTextView;
        //TextView itemShortDescTextView=(TextView)convertView.findViewById(R.id.item_short_desc_textview);
        TextView itemQuantityTextView;
        TextView itemCostTextView;
        TextView itemDescTextView;
        //CheckBox itemConfirmedCheckBox;
        ImageView imageView;
    }

    public PendingOrderDetailsAdapter(Activity activity, ArrayList<PendingOrderDetailsSnippet> pendingOrderDetails) {
        m_activity = activity;
        m_pendingOrderDetails = pendingOrderDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_pendingOrderDetails != null) {
            count = m_pendingOrderDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_pendingOrderDetails != null) {
            return m_pendingOrderDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_pendingOrderDetails != null) {
            return m_pendingOrderDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_pendingOrderDetails != null) {

            final ViewHolder holder;

            String itemCode = ((PendingOrderDetailsSnippet) getItem(position)).m_itemCode;
            String itemName = ((PendingOrderDetailsSnippet) getItem(position)).m_itemName;
            String itemShortDesc = ((PendingOrderDetailsSnippet) getItem(position)).m_itemShortDesc;
            int itemQuantity = ((PendingOrderDetailsSnippet) getItem(position)).m_itemQuantity;
            double itemCost = ((PendingOrderDetailsSnippet) getItem(position)).m_itemCost;
            String itemDesc = ((PendingOrderDetailsSnippet) getItem(position)).m_itemDesc;
            String itemImageAdd = ((PendingOrderDetailsSnippet) getItem(position)).m_itemImgAdd;

            final int pos = position;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.pending_order_details_display_list, (ViewGroup) null);
                //TextView itemCodeTextView=(TextView)convertView.findViewById(R.id.item_code_textview);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                //TextView itemShortDescTextView=(TextView)convertView.findViewById(R.id.item_short_desc_textview);
                holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                //holder.itemConfirmedCheckBox = (CheckBox) convertView.findViewById(R.id.item_confirmed_checbox);
                holder.imageView = (ImageView)convertView.findViewById(R.id.pending_item_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
                //holder.itemConfirmedCheckBox.setOnCheckedChangeListener(null);
            }


            //itemCodeTextView.setText(itemCode);
            holder.itemNameTextView.setText(itemName);
            //itemShortDescTextView.setText(itemShortDesc);
            holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
            holder.itemCostTextView.setText(Double.toString(itemCost));
            holder.itemDescTextView.setText(itemDesc);

            if(itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.abc_btn_check_material)
                    .into(holder.imageView);


            Picasso.with(parent.getContext())
                    .setIndicatorsEnabled(true);

            /*
            holder.itemConfirmedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((PendingOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 1;
                    } else {
                        ((PendingOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 0;
                    }
                }
            });*/


        }
        return convertView;
    }

}
