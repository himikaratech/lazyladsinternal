package technologies.angular.lazyladinternal;

import java.io.IOException;

/**
 * Created by Saurabh on 29/01/15.
 */
public class ServerResponseException extends IOException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ServerResponseException(String string) {
        super(string);
    }

}
