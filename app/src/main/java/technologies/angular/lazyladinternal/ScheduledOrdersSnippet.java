package technologies.angular.lazyladinternal;

/**
 * Created by Sakshi Gupta on 03-07-2015.
 */
public class ScheduledOrdersSnippet {

    public int scheduledOrdersId;
    public String m_scheduledOrderCode;
    public String m_address;
    public String m_userCode;
    public String m_timeOrderPlaced;
    public Long m_expectedTime;
    public int m_amount;
    public String m_userName;
    public String m_userPhoneNum;
    public String m_orderStatus;
    public String m_spName;
    public String m_coupon;
    public int m_delBoyRequested;
    public int m_delBoyAssigned;
    public int m_delBoyConfirmed;
    public String m_verifiedNumber;

    public ScheduledOrdersSnippet(){
    }

    public ScheduledOrdersSnippet(int id, String scheduledOrderCode, String customer_address, String user_code, String time_order_placed, Long expected_time, int total_amount, String userName, String userPhoneNum, String order_status, String spName, String coupon, String verified_number, int delBoyRequested, int delBoyAssigned, int delBoyConfirmed){
        scheduledOrdersId=id;
        m_scheduledOrderCode=scheduledOrderCode;
        m_address=customer_address;
        m_userCode=user_code;
        m_timeOrderPlaced=time_order_placed;
        m_expectedTime=expected_time;
        m_amount=total_amount;
        m_userName=userName;
        m_userPhoneNum=userPhoneNum;
        m_orderStatus=order_status;
        m_spName=spName;
        m_coupon=coupon;
        m_delBoyRequested = delBoyRequested;
        m_delBoyAssigned = delBoyAssigned;
        m_delBoyConfirmed = delBoyConfirmed;
        m_verifiedNumber = verified_number;
    }
}
