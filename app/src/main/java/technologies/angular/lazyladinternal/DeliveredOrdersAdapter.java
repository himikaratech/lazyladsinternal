package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Saurabh on 30/01/15.
 */
public class DeliveredOrdersAdapter extends BaseAdapter {

    private ArrayList<DeliveredOrdersSnippet> m_deliveredOrdersDetails;
    private Activity m_activity;

    public DeliveredOrdersAdapter(Activity activity, ArrayList<DeliveredOrdersSnippet> deliveredOrdersDetails) {
        m_activity = activity;
        m_deliveredOrdersDetails = deliveredOrdersDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_deliveredOrdersDetails != null) {
            count = m_deliveredOrdersDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_deliveredOrdersDetails != null) {
            return m_deliveredOrdersDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_deliveredOrdersDetails != null) {
            return m_deliveredOrdersDetails.get(position).deliveredOdersId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_deliveredOrdersDetails != null) {
            final String delOrderCode = ((DeliveredOrdersSnippet) getItem(position)).orderCode;
            String delOrderAddress = ((DeliveredOrdersSnippet) getItem(position)).address;
            String delOrderTimes = ((DeliveredOrdersSnippet) getItem(position)).timeOrderPlaced;
            String delOrderUserName = ((DeliveredOrdersSnippet) getItem(position)).userName;
            String delOrderPhoneNumber = ((DeliveredOrdersSnippet) getItem(position)).userPhone;
            String delOrderCoupon = ((DeliveredOrdersSnippet) getItem(position)).m_coupon;

            double delOrderTotalAmount = ((DeliveredOrdersSnippet) getItem(position)).amount;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.delivered_orders_display_list, (ViewGroup) null);
            }

            TextView delOrderCodeTextView = (TextView) convertView.findViewById(R.id.del_order_code_textview);
            TextView delOrderAddressTextView = (TextView) convertView.findViewById(R.id.del_order_address_textview);
            TextView delOrderTimesTextView = (TextView) convertView.findViewById(R.id.del_order_time_placed_textview);
            TextView delOrderTotalAmountTextView = (TextView) convertView.findViewById(R.id.amount_del_order_textview);
            LinearLayout header = (LinearLayout) convertView.findViewById(R.id.head_order);
            TextView delOrderUserNameTextView = (TextView) convertView.findViewById(R.id.del_order_username_textview);
            TextView delOrderUserPhoneTextView = (TextView) convertView.findViewById(R.id.del_order_number_textview);
            TextView delOrderCouponTextView =(TextView)convertView.findViewById(R.id.del_coupon_code_textview);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            header.setBackgroundColor(color);

            delOrderCodeTextView.setText(delOrderCode);
            delOrderAddressTextView.setText(delOrderAddress);
            delOrderTotalAmountTextView.setText("₹ " + Double.toString(delOrderTotalAmount));
            delOrderTimesTextView.setText(delOrderTimes);
            delOrderUserNameTextView.setText(delOrderUserName);
            delOrderUserPhoneTextView.setText(delOrderPhoneNumber);
            delOrderCouponTextView.setText(delOrderCoupon);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDeliveredOrderDetilsActivity(delOrderCode);
                }
            });
        }

        return convertView;
    }

    private void startDeliveredOrderDetilsActivity(String deliveredOrderCode){
        Intent intent = new Intent(m_activity, DeliveredOrderDetails.class);
        String data[] = {deliveredOrderCode};
        intent.putExtra("data_send_delivered_details", data);
        m_activity.startActivity(intent);
    }
}
