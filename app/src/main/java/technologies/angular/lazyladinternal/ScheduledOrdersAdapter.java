package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Sakshi Gupta on 03-07-2015.
 */
public class ScheduledOrdersAdapter extends BaseAdapter {

    private class ViewHolder {

      //  Button cancel_order_button;
        Button delivery_boy;
      //  TextView orderCancelled;
        ImageView requestedFlagImage;
        ImageView assignedFlagImage;
        ImageView confirmedFlagImage;
        int riderFlag = 0;
    }
    private ArrayList<ScheduledOrdersSnippet> m_scheduledOrdersDetails;

    private Activity m_activity;

    public ScheduledOrdersAdapter(Activity activity, ArrayList<ScheduledOrdersSnippet> scheduledOrdersDetails){
        m_activity=activity;
        m_scheduledOrdersDetails=scheduledOrdersDetails;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_scheduledOrdersDetails != null){
            count=m_scheduledOrdersDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_scheduledOrdersDetails != null){
            return m_scheduledOrdersDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_scheduledOrdersDetails != null){
            return m_scheduledOrdersDetails.get(position).scheduledOrdersId;
        }
        return 0;
    }

    public static String GetHumanReadableDate(long epochSec, String dateFormatStr) {
        Date date = new Date(epochSec * 1000);
        SimpleDateFormat format = new SimpleDateFormat(dateFormatStr);
        return format.format(date);

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(m_scheduledOrdersDetails!=null){

            final ViewHolder holder;
            final int positionFinal = position;
            final String scheduledOrderCode=((ScheduledOrdersSnippet) getItem(position)).m_scheduledOrderCode;
            String scheduledOrderAddress=((ScheduledOrdersSnippet) getItem(position)).m_address;
            int scheduledOrderAmount=((ScheduledOrdersSnippet) getItem(position)).m_amount;
            String scheduledOrderTimePlaced=((ScheduledOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            Long engTime = ((ScheduledOrdersSnippet) getItem(position)).m_expectedTime;
            String scheduledOrderTimeExpected = GetHumanReadableDate(engTime, "E LLL dd, yyyy HH:mm");
            String scheduledOrderUserName = ((ScheduledOrdersSnippet)getItem(position)).m_userName;
            String scheduledOrderPhoneNumber = ((ScheduledOrdersSnippet)getItem(position)).m_userPhoneNum;
            final String userCode=((ScheduledOrdersSnippet) getItem(position)).m_userCode;
            String orderStatus = ((ScheduledOrdersSnippet) getItem(position)).m_orderStatus;
            String scheduledOrderShopName = ((ScheduledOrdersSnippet) getItem(position)).m_spName;
            String scheduledOrderCoupon = ((ScheduledOrdersSnippet) getItem(position)).m_coupon;
            int deliveryBoyRequested = ((ScheduledOrdersSnippet) getItem(position)).m_delBoyRequested;
            int deliveryBoyAssigned = ((ScheduledOrdersSnippet) getItem(position)).m_delBoyAssigned;
            int deliveryBoyConfirmed = ((ScheduledOrdersSnippet) getItem(position)).m_delBoyAssigned;
            String scheduledOrderVerifiedNumber = ((ScheduledOrdersSnippet) getItem(position)).m_verifiedNumber;

            if(convertView == null){
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.scheduled_orders_display_list, (ViewGroup)null);
           //     holder.cancel_order_button = (Button) convertView.findViewById(R.id.cancel_prev_order_button);
            //    holder.orderCancelled = (TextView) convertView.findViewById(R.id.order_cancelled);
                holder.delivery_boy = (Button) convertView.findViewById(R.id.deliveryBoy);
                holder.requestedFlagImage = (ImageView) convertView.findViewById(R.id.requestedFlagImage);
                holder.assignedFlagImage = (ImageView) convertView.findViewById(R.id.assignedFlagImage);
                holder.confirmedFlagImage = (ImageView) convertView.findViewById(R.id.confirmedFlagImage);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder)convertView.getTag();
            }
            TextView scheduledUserCodeTextView=(TextView)convertView.findViewById(R.id.scheduled_user_code_textview);
            TextView scheduledOrderCodeTextView=(TextView)convertView.findViewById(R.id.scheduled_order_code_textview);
            TextView scheduledOrderAddressTextView=(TextView)convertView.findViewById(R.id.scheduled_order_address_textview);
            TextView scheduledOrderAmountTextView=(TextView)convertView.findViewById(R.id.amount_scheduled_order_textview);
            TextView scheduledOrderTimePlacedTextView=(TextView)convertView.findViewById(R.id.scheduled_order_del_textview);
            TextView scheduledOrderTimeExpectedTextView=(TextView)convertView.findViewById(R.id.scheduled_order_exp_textview);
            TextView scheduledOrderUserNameTextView =(TextView)convertView.findViewById(R.id.scheduled_order_username_textview);
            TextView scheduledOrderUserPhoneTextView =(TextView)convertView.findViewById(R.id.scheduled_order_number_textview);
            TextView scheduledOrderShopNameTextView =(TextView)convertView.findViewById(R.id.scheduled_order_shopname_textview);
            TextView scheduledOrderCouponTextView =(TextView)convertView.findViewById(R.id.scheduled_coupon_code_textview);
            TextView scheduledVerifiedNumberTextView=(TextView)convertView.findViewById(R.id.scheduled_verified_number_textview);

            LinearLayout header =(LinearLayout)convertView.findViewById(R.id.head_order);
          //  holder.orderCancelled.setText("");
          //  holder.cancel_order_button.setVisibility(View.VISIBLE);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            header.setBackgroundColor(color);

              if (((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1) {
                holder.riderFlag = 1;
            }
            else {
                holder.riderFlag = 0;
            }

            if (((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1) {
                if(((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1){
                    holder.delivery_boy.setVisibility(View.VISIBLE);
                }
                else
                    holder.delivery_boy.setVisibility(View.VISIBLE);
            }
            else
                holder.delivery_boy.setVisibility(View.INVISIBLE);

            scheduledUserCodeTextView.setText(userCode);
            scheduledOrderCodeTextView.setText(scheduledOrderCode);
            scheduledOrderAddressTextView.setText(scheduledOrderAddress);
            scheduledOrderAmountTextView.setText("₹ " + Integer.toString(scheduledOrderAmount));
            scheduledOrderTimePlacedTextView.setText(scheduledOrderTimePlaced);
            scheduledOrderTimeExpectedTextView.setText(scheduledOrderTimeExpected);
            scheduledOrderUserNameTextView.setText(scheduledOrderUserName);
            scheduledOrderUserPhoneTextView.setText(scheduledOrderPhoneNumber);
            scheduledOrderShopNameTextView.setText(scheduledOrderShopName);
            scheduledOrderCouponTextView.setText(scheduledOrderCoupon);
            scheduledVerifiedNumberTextView.setText(scheduledOrderVerifiedNumber);

            if (((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1)
                holder.requestedFlagImage.setImageResource(R.drawable.active);
            else
                holder.requestedFlagImage.setImageResource(R.drawable.inactive);

            if(((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyAssigned == 1)
                holder.assignedFlagImage.setImageResource(R.drawable.active);
            else
                holder.assignedFlagImage.setImageResource(R.drawable.inactive);

            if(((ScheduledOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1)
                holder.confirmedFlagImage.setImageResource(R.drawable.active);
            else
                holder.confirmedFlagImage.setImageResource(R.drawable.inactive);

        /*    if (orderStatus.equals("Cancelled")) {
                holder.orderCancelled.setText("Cancelled");
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }
            */

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if((((ScheduledOrdersSnippet) getItem(positionFinal)).m_orderStatus).equals("Pending"))
                    startCurrentOrderDetilsActivity(scheduledOrderCode, userCode);

                    else if((((ScheduledOrdersSnippet) getItem(positionFinal)).m_orderStatus).equals("Confirmed"))
                    startPendingOrderDetilsActivity(scheduledOrderCode, userCode);
                   }
            });


            holder.delivery_boy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent (m_activity, AssignOrChangeDeliveryBoy.class);
                    String order_code = scheduledOrderCode;
                    in.putExtra("order_code", order_code);
                    in.putExtra("riderFlag", holder.riderFlag);
                    Log.i("rider flag", holder.riderFlag + "");
                    m_activity.startActivity(in);
                }
            });

        /*    holder.cancel_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelThisOrder(scheduledOrderCode);
                    ((ScheduledOrdersSnippet) getItem(positionFinal)).m_orderStatus = "Cancelled";
                    holder.orderCancelled.setText("Cancelled");
                    holder.cancel_order_button.setVisibility(View.INVISIBLE);
                }
            });
            */
        }
        return convertView;
    }

    private void startCurrentOrderDetilsActivity(String currentOrderCode, String userCode){
        Intent intent = new Intent(m_activity, CurrentOrderDetails.class);
        String data[] = {currentOrderCode, userCode};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }

    private void startPendingOrderDetilsActivity(String pendingOrderCode, String userCode){
        Intent intent = new Intent(m_activity, PendingOrderDetails.class);
        String data[] = {pendingOrderCode, userCode};
        intent.putExtra("data_send_pending_details", data);
        m_activity.startActivity(intent);
    }

    private void cancelThisOrder(String order_code){
        String url_cancel_order = "http://www.angulartechnologies.com/task_manager/v1/cancelUsersPreviousOrder";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_code", order_code);

        JsonObjectRequest sendOrderDetails = new JsonObjectRequest(url_cancel_order, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("error");
                            if (success == 1) {

                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(m_activity);
        m_sessionManager.addToRequestQueue(sendOrderDetails);
    }
}
