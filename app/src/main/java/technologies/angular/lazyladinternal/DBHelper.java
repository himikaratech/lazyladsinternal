package technologies.angular.lazyladinternal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Saurabh on 11/02/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static int m_currentDBVersion=2;
    private static String m_DB_NAME = "LazyLadServProvData";
    private SQLiteDatabase m_db;
    private static DBHelper dbinstance;
    private static int[] version;

    private static final String USER_DETAILS = "service_providers";
    private static final String ITEMS_NOT_AVAILABLE = "items_not_available";
    private static final String ITEMS_AVAILABLE = "items_available";
    private static final String ITEMS_OUT_OF_STOCK = "items_out_of_stock";
    private static final String SEARCH_RESULT="search_result";

    static {
        DBHelper.dbinstance = null;
        DBHelper.version = null;
    }

    public DBHelper(Context context) {
        super(context, m_DB_NAME, (CursorFactory)(null), DBHelper.m_currentDBVersion);
        //Log.i("In DBHelper", "Class");
    }

    public DBHelper(Context context, String name, CursorFactory factory,
                    int version) {
        super(context, name, factory, DBHelper.m_currentDBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE IF NOT EXISTS " + USER_DETAILS + "(id INTEGER PRIMARY KEY, sp_code text, sp_email text, " +
                "sp_password text);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ITEMS_NOT_AVAILABLE + "(id INTEGER PRIMARY KEY, item_code text, item_name text, " +
                "item_img_flag int, item_img_add text, item_unit text, item_short_desc text, item_desc text, item_status int, item_cost double, " +
                "item_type_code text, item_quantity text);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ITEMS_OUT_OF_STOCK + "(id INTEGER PRIMARY KEY, item_code text, item_name text, " +
                "item_img_flag int, item_img_add text, item_unit text, item_short_desc text, item_desc text, item_status int, item_cost double, " +
                "item_type_code text, item_quantity text);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ITEMS_AVAILABLE + "(id INTEGER PRIMARY KEY, item_code text, item_name text, " +
                "item_img_flag int, item_img_add text, item_unit text, item_short_desc text, item_desc text, item_status int, item_cost double, item_type_code text, item_quantity text);");

        //Version 2 created table
        db.execSQL("CREATE TABLE IF NOT EXISTS " + SEARCH_RESULT + "(id INTEGER AUTO_INCREMENT PRIMARY KEY," +
                "order_id text, order_code text, order_status text, order_address text, user_code text, time_order_placed text," +
                "expected_time int, amount int, user_name text, user_phone text,  sp_name text," +
                " seller_time int);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(2 > oldVersion && 2<= newVersion) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + SEARCH_RESULT + "(id INTEGER AUTO_INCREMENT PRIMARY KEY," +
                    "order_id text, order_code text, order_status text,order_address text, user_code text, time_order_placed text," +
                    "expected_time int, amount int, user_name text, user_phone text,  sp_name text," +
                    "seller_time int);");

        }
    }
}
