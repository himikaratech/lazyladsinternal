package technologies.angular.lazyladinternal;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Saurabh on 29/01/15.
 */
public class HTTPConnectionHandler {

    private HttpClient httpclient;

    public HTTPConnectionHandler() {
        HttpParams my_httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(my_httpParams, 5000);
        HttpConnectionParams.setSoTimeout(my_httpParams, 10000);

        this.httpclient = new DefaultHttpClient(my_httpParams);
    }

    public String getResponse(String urlFinder) throws ServerResponseException, IOException {
        HttpResponse httpResponse = this.httpclient.execute((HttpUriRequest)(new HttpGet(urlFinder)));
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() == 200) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            httpResponse.getEntity().writeTo((OutputStream)(byteArrayOutputStream));
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toString();
        }
        httpResponse.getEntity().getContent().close();
        throw new ServerResponseException(statusLine.getReasonPhrase());
    }

    public String postResponse(String urlFinder, ArrayList<NameValuePair> arrayList) throws ServerResponseException, IOException {
        HttpPost httpPost = new HttpPost(urlFinder);
        //httpPost.setHeader();
        httpPost.setEntity((HttpEntity)(new UrlEncodedFormEntity(arrayList)));
        HttpResponse httpResponse = this.httpclient.execute((HttpUriRequest)(httpPost));
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() == 200) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            httpResponse.getEntity().writeTo((OutputStream)(byteArrayOutputStream));
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toString();
        }
        httpResponse.getEntity().getContent().close();
        throw new ServerResponseException((String.valueOf((Object)(statusLine.getReasonPhrase())) + " URL:" + urlFinder + " data:" + arrayList.toString()));
    }

}
