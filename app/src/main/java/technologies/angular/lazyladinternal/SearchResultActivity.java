package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jpardogo.android.googleprogressbar.library.FoldingCirclesDrawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 03-08-2015.
 */
public class SearchResultActivity extends ActionBarActivity {

    private ListView m_orderDetailsListView;
    private SearchResultAdapter m_orderDetailAdapter;
    private ProgressDialog progressDialog;
    String API ="http://www.angulartechnologies.com/task_manager/v1";
    private Toolbar toolbar;
    private Intent m_searchIntent;
    private String m_query = null;
    private String m_orderCode;
    private ArrayList<SearchResultSnippet> m_searchOrderDetailsArrayList;
    private SearchResultAdapter m_searchResultsAdapter;
    private AsyncTask m_async5;
    public static ContentResolver m_conRes=null;
    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;

    private Boolean doneLoading = true;
    private Boolean shallLoad = true;

    static{
        SearchResultActivity.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    static{
        SearchResultActivity.deleteUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
    }
    static{
        SearchResultActivity.insertUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
    }
    static{
        SearchResultActivity.updateUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_for_orders);

        toolbar = (Toolbar) findViewById(R.id.items_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("New Orders");
        m_conRes = this.getContentResolver();
        m_orderDetailsListView = (ListView) findViewById(R.id.search_result_list_view);

        m_searchIntent = getIntent();

        handleIntent(m_searchIntent);

        if(Intent.ACTION_SEARCH.equals(m_searchIntent.getAction())){
            m_orderDetailsListView.setOnScrollListener(new LL_ScrollListener());
        }
    }


    private void getMoreSearchedItemsFromServerLimited(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminateDrawable(new FoldingCirclesDrawable.Builder(this).build());
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_bar_for_more_search_results);

        Log.e("load retro", "loading");
        JSONObject jObject = null;
        String previous_order_code;
        if(null == m_searchOrderDetailsArrayList || m_searchOrderDetailsArrayList.isEmpty())
            previous_order_code = "0";
        else
            previous_order_code = m_searchOrderDetailsArrayList.get(m_searchOrderDetailsArrayList.size()-1).m_orderCode;
        Log.i("previous order code", previous_order_code);

        SharedPreferences sprefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
        try {
            jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Jobject at search",jObject.toString());
        Log.i("m_query",m_query);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        SearchApi searchService = restAdapter.create(SearchApi.class);
        SearchApi.InputForSearchOrders search = new SearchApi.InputForSearchOrders(m_query, previous_order_code, jObject.toString());

       searchService.searchFeeds(search, new Callback<SearchApi.SearchModel>() {

            @Override
            public void success(SearchApi.SearchModel searchResponse, Response response) {
                int success = searchResponse.error;
                Log.e("sucess Search", searchResponse.error + "");
                if (success == 1) {
                    ArrayList<SearchResultSnippet> orderList = new ArrayList<SearchResultSnippet>();
                    orderList = searchResponse.orders_service_providers;
                    Log.i("Response result", searchResponse.toString());
                    if (null != orderList && !orderList.isEmpty()) {

                        m_async5 = new StoreItemsInSqliteandLoad(m_query).execute(orderList);
                    } else
                        shallLoad = false;
                }
                else {
                    Toast.makeText(getApplicationContext(), "No result Found", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        m_orderDetailsListView.setOnScrollListener(null);

        m_searchOrderDetailsArrayList = null;
        handleIntent(intent);

        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            m_orderDetailsListView.setOnScrollListener(new LL_ScrollListener());
        }
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            m_query = intent.getStringExtra(SearchManager.QUERY);

            String previous_order_code;
            if (null == m_searchOrderDetailsArrayList || m_searchOrderDetailsArrayList.isEmpty())
                previous_order_code = "0";
            else
                previous_order_code = m_searchOrderDetailsArrayList.get(m_searchOrderDetailsArrayList.size() - 1).m_orderCode;

            String query = "select * from search_result";
          //  String newest_item_condition = " and CAST(order_code as INTEGER) > " + previous_order_code + " ";
            String final_query = query;
            Log.i("Final query", final_query);
            Cursor cursor = SearchResultActivity.m_conRes.query(SearchResultActivity.selectUri, SearchResultActivity.dummy,
                    final_query, SearchResultActivity.dummy, "");
            //cursor.moveToFirst();
            //String std= cursor.getString(3);
            //Log.i("cursor", std);
            Log.i("Cursor values", cursor.getExtras().toString());
            Log.d("cursor count", cursor.getCount() + "");
            processCursorData(cursor);

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            m_orderCode = intent.getDataString();
            Cursor cursor = SearchResultActivity.m_conRes.query(SearchResultActivity.selectUri, SearchResultActivity.dummy,
                    "select * from search_result where order_code = '" + m_orderCode + "' limit 1 ", SearchResultActivity.dummy, "");
            Log.i("cursor on view click", cursor.getCount()+"");
            processCursorData(cursor);
        }
    }

    private void processCursorData(Cursor cursor) {
        //cursor.moveToFirst();
        //String std= cursor.getString(3);
        //Log.i("cursor", std);
        ArrayList<SearchResultSnippet> searchOrderDetailsArrayList = new ArrayList<SearchResultSnippet>();
        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {

                    int orderId = cursor.getInt(1);
                    String orderCode = cursor.getString(2);
                    String address = cursor.getString(4);
                    String userCode = cursor.getString(5);
                    String timeOrderPlaced = cursor.getString(6);
                    Long expectedTime = cursor.getLong(7);
                    int amount = cursor.getInt(8);
                    String userName = cursor.getString(9);
                    String userPhoneNum = cursor.getString(10);
                    String orderStatus = cursor.getString(3);
                    String spName = cursor.getString(11);
                    Long sellerDelTime = cursor.getLong(12);
                    Log.i("SearchResultActivty", orderCode);
                    Log.i("SearchResultActivty", orderStatus);
                    SearchResultSnippet m_itemSnipObj = new SearchResultSnippet(orderId, orderCode, address, userCode , timeOrderPlaced, expectedTime, amount, userName, userPhoneNum, orderStatus, spName, sellerDelTime);
                    searchOrderDetailsArrayList.add(m_itemSnipObj);
                    cursor.moveToNext();
                }
            }
        }
        if (cursor != null) cursor.close();

        if (null == m_searchOrderDetailsArrayList) {
            m_searchOrderDetailsArrayList = searchOrderDetailsArrayList;
            m_searchResultsAdapter = new SearchResultAdapter(SearchResultActivity.this, m_searchOrderDetailsArrayList);
            m_orderDetailsListView.setAdapter((ListAdapter) m_searchResultsAdapter);
        } else {
            m_searchOrderDetailsArrayList.addAll(searchOrderDetailsArrayList);
            m_searchResultsAdapter.notifyDataSetChanged();
        }
        if(!doneLoading)
            doneLoading = true;
    }

    class StoreItemsInSqliteandLoad extends AsyncTask<ArrayList<SearchResultSnippet>, Void, Cursor> {

        String searchText;

        public StoreItemsInSqliteandLoad(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Cursor doInBackground(ArrayList<SearchResultSnippet>... params) {
            ArrayList<SearchResultSnippet> orderList = params[0];

            storeItemsinSqliteForQuery(orderList, searchText);
            String newest_item_condition;
            String previous_order_code;
            if(null == m_searchOrderDetailsArrayList || m_searchOrderDetailsArrayList.isEmpty()) {
               // previous_order_code = "0";
                newest_item_condition = " ";
            }
            else
            {
                previous_order_code = m_searchOrderDetailsArrayList.get(m_searchOrderDetailsArrayList.size()-1).m_orderCode;
                newest_item_condition = " and CAST(order_code as INTEGER) < "+previous_order_code+" ";
            }
            String query = "select * from search_result where 1";
             String final_query = query + newest_item_condition;

            Cursor cursor = SearchResultActivity.m_conRes.query(SearchResultActivity.selectUri, SearchResultActivity.dummy,
                    final_query, SearchResultActivity.dummy, "");

            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            processCursorData(cursor);
        }
    }

    public void storeItemsinSqliteForQuery(ArrayList<SearchResultSnippet> orderList, String searchText){

        String searchTextM = Utils.strForSqlite(searchText);
        for (int i = 0; i < orderList.size(); i++) {
            int ordersId = orderList.get(i).ordersId;
            String orderCode = orderList.get(i).m_orderCode;
            String address = Utils.strForSqlite(orderList.get(i).m_address);
            String userCode = orderList.get(i).m_userCode;
            String timeOrderPlaced = orderList.get(i).m_timeOrderPlaced;
            Long expectedTime = orderList.get(i).m_expectedTime;
            int amount = orderList.get(i).m_amount;
            String userName = Utils.strForSqlite(orderList.get(i).m_userName);
            String userPhoneNum = orderList.get(i).m_userPhoneNum;
            String orderStatus = orderList.get(i).m_orderStatus;
            String spName = Utils.strForSqlite(orderList.get(i).m_spName);
            Long sellerDelTime = orderList.get(i).m_sellerTime;
            m_conRes.query(TabViewForOrders.insertUri, dummy, "insert into search_result (order_id, order_code, order_address, " +
                    "user_code, time_order_placed, expected_time, amount, user_name, user_phone, order_status, " +
                    "sp_name, seller_time) values ('" + ordersId + "', '" + orderCode + "', '" + address + "', '" + userCode + "', " +
                    "'" + timeOrderPlaced + "', '" + expectedTime + "', '" + amount + "', '" + userName + "', '" + userPhoneNum + "', " +
                    "'" + orderStatus + "', '" + spName + "', '" + sellerDelTime + "')", dummy, "");
        }
    }

    @Override
    public void onBackPressed(){
        SearchResultActivity.this.finish();
        super.onBackPressed();
    }

    private class LL_ScrollListener implements AbsListView.OnScrollListener {
        int visibleThreshold = 1;
        int visibleItemCount;
        int totalItemCount;
        int firstVisibleItem;


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

            if (scrollState == SCROLL_STATE_IDLE) {
                if (shallLoad && doneLoading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    doneLoading = false;
                    getMoreSearchedItemsFromServerLimited();
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            this.firstVisibleItem = firstVisibleItem;
            this.visibleItemCount = visibleItemCount;
            this.totalItemCount = totalItemCount;
            if(this.totalItemCount == 0)
                onScrollStateChanged(view,SCROLL_STATE_IDLE);
        }
    }
}
