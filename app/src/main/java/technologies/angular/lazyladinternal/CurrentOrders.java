package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CurrentOrders extends Fragment {

    private Boolean dataLoading = false;
    //private String m_servProvId;
    private int m_numberOfCurrentOrders;
    private ArrayList<CurrentOrdersSnippet> m_currentOrdersList = null;
    private CurrentOrdersAdapter m_currentOrdersAdapter = null;
    private ListView m_currentOrdersDetails;

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private int[] mIcon;


    public static final String EXTRA_MESSAGE = "message";

    private Cursor m_cursor;
    private static ContentResolver m_conRes=null;
    private static String[] dummy = new String[]{"a", "b"};
    private static Uri selectUri;

    static{
        CurrentOrders.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.current_orders,container,false);

        if(!Utils.isNetworkAvailable(getActivity())){
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }
            m_conRes=getActivity().getContentResolver();
            m_currentOrdersDetails = (ListView)rootView.findViewById(R.id.current_orders_list_view);

            m_currentOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                    if (null == m_currentOrdersDetails)
                        return;

                    int threshold = 1;
                    int count = m_currentOrdersDetails.getCount();

                    if (scrollState == SCROLL_STATE_IDLE) {
                        if (!dataLoading && m_currentOrdersDetails.getLastVisiblePosition() >= count - threshold) {

                            if(Utils.isNetworkAvailable(getActivity())) {
                                dataLoading = true;
                                new LoadCurrentOrders().execute();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem,
                            int visibleItemCount, int totalItemCount) {
                                //leave this empty
                            }
            });

        new LoadCurrentOrders().execute();
        return rootView;
        }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_orders, menu);
        return true;
    }



    /**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("yjfhv", "hjgchg");
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getActivity());
            newMessage="love is life";
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            Toast.makeText(getActivity(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();

            new LoadCurrentOrders().execute();

        }
    };

    class LoadCurrentOrders extends AsyncTask<String, String, ArrayList<CurrentOrdersSnippet>> {

        private ProgressDialog pDialog;
        private static final String url_currentOrders_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/current_order_internal_lazylad";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading Orders. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected ArrayList<CurrentOrdersSnippet> doInBackground(String... params) {
            String response_currentOrders_servProv;
            JSONObject json_data_currentOrders_servProv;
            JSONArray currentOrders_servProv;
            HTTPConnectionHandler hTTPConnection_currentOrders_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_currentOrders_ServProv = new ArrayList();

            //String servProvId=m_servProvId;     //"1";
            //arrayList_currentOrders_ServProv.add((Object)new BasicNameValuePair("servProv_id", servProvId));

            String orderId;
            JSONObject jObject = null;
            if(m_currentOrdersList == null)
            {
                orderId = "0";
            }
            else
            {

                orderId = m_currentOrdersList.get(m_currentOrdersList.size() - 1).m_currentOrderCode;
            }

            SharedPreferences sprefs = getActivity().getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
            try {
                jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
                Log.i("send to server", jObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            arrayList_currentOrders_ServProv.add((Object)new BasicNameValuePair("before_order_code", orderId));
            arrayList_currentOrders_ServProv.add((Object)new BasicNameValuePair("order_filter", jObject.toString()));

            try{
                Log.i("In Asynch", "Step2");
                response_currentOrders_servProv=hTTPConnection_currentOrders_ServProv.postResponse(url_currentOrders_serviceProvider, arrayList_currentOrders_ServProv);
                Log.i("Response", response_currentOrders_servProv);
                if(response_currentOrders_servProv==null){
                    return null;
                }
                else{
                    json_data_currentOrders_servProv=new JSONObject(response_currentOrders_servProv);
                    try{
                       int success = json_data_currentOrders_servProv.getInt("error");
                        if(success == 1){
                            currentOrders_servProv=json_data_currentOrders_servProv.getJSONArray("current_orders_service_providers");
                            int numberOfCurrentOrders=currentOrders_servProv.length();
                            ArrayList<CurrentOrdersSnippet> currentOrdersList = new ArrayList<CurrentOrdersSnippet>(numberOfCurrentOrders);

                            for(int i=0; i<numberOfCurrentOrders; i++){
                                JSONObject current_orders_list_object = currentOrders_servProv.getJSONObject(i);
                                int currentOrderId=i+1;
                                String currentOrderCode=current_orders_list_object.getString("current_order_code");
                                String address=current_orders_list_object.getString("delivery_address");
                                String userCode = Integer.toString(current_orders_list_object.getInt("user_code"));
                                String timeOrderPlaced=current_orders_list_object.getString("order_time");
                                int amount = current_orders_list_object.getInt("total_amount");
                                String userName=current_orders_list_object.getString("user_name");
                                String userPhoneNum=current_orders_list_object.getString("user_phone_number");
                                String spName=current_orders_list_object.getString("sp_name");
                                String coupon=current_orders_list_object.getString("coupon_code");
                                String verifiedNumber = current_orders_list_object.getString("phone_number");
                                int deliveryBoyRequested = current_orders_list_object.getInt("rider_requested");
                                int deliveryBoyAssigned = current_orders_list_object.getInt("rider_assigned");
                                int deliveryBoyConfirmed = current_orders_list_object.getInt("rider_accepted");
                                //  int deliveryBoyRequested = 1;
                                // int deliveryBoyAssigned = 0;
                                // int deliveryBoyConfirmed = 0;

                                CurrentOrdersSnippet currenOrderObj = new CurrentOrdersSnippet(currentOrderId, currentOrderCode, address, userCode, timeOrderPlaced, amount, userName, userPhoneNum, spName, coupon, verifiedNumber, deliveryBoyRequested, deliveryBoyAssigned, deliveryBoyConfirmed);

                                currentOrdersList.add(currenOrderObj);
                            }
                                 return currentOrdersList;
                        }else{
                                 return null;
                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(ArrayList<CurrentOrdersSnippet> currentOrdersList) {

               if (null != currentOrdersList) {
                   if (m_currentOrdersList == null) {
                       m_currentOrdersList = currentOrdersList;
                       m_currentOrdersAdapter = new CurrentOrdersAdapter(getActivity(), m_currentOrdersList);
                       m_currentOrdersDetails.setAdapter((ListAdapter) m_currentOrdersAdapter);
                       Log.i("In Asynch", "currentempty");
                   } else {
                       m_currentOrdersList.addAll(currentOrdersList);
                       m_currentOrdersAdapter.notifyDataSetChanged();
                   }
               }

              pDialog.dismiss();
              dataLoading = false;
        }
    }

    @Override
    public void onStart(){
        super.onStart();
     //   m_currentOrdersList = null;
       //     new LoadCurrentOrders().execute();
    }
    }
