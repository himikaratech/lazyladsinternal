package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sakshi Gupta on 03-07-2015.
 */
public class ScheduledOrders extends Fragment {
        private Boolean dataLoading = false;

        private int m_numberOfScheduledOrders;
        private ArrayList<ScheduledOrdersSnippet> m_scheduledOrdersList = null;
        private ScheduledOrdersAdapter m_scheduledOrdersAdapter=null;
        private ListView m_scheduledOrdersDetails;

        private List<String> listDataHeader;
        private HashMap<String, List<String>> listDataChild;
        private int[] mIcon;
        public static final String EXTRA_MESSAGE = "message";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.scheduled_orders,container,false);

        if(!Utils.isNetworkAvailable(getActivity())){
            Toast.makeText(getActivity(), "Please Check Your Network Connectivity", Toast.LENGTH_SHORT).show();
        }
                m_scheduledOrdersDetails = (ListView)rootView.findViewById(R.id.scheduled_orders_list_view);
                m_scheduledOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                        if (null == m_scheduledOrdersDetails)
                            return;

                        int threshold = 1;
                        int count = m_scheduledOrdersDetails.getCount();

                        if (scrollState == SCROLL_STATE_IDLE) {
                            if (!dataLoading && m_scheduledOrdersDetails.getLastVisiblePosition() >= count - threshold) {
                                if (Utils.isNetworkAvailable(getActivity())) {

                                    dataLoading = true;
                                    // Execute LoadMoreDataTask AsyncTask
                                    new LoadScheduledOrders().execute();
                                } else {
                                    Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                              }
                            }
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem,
                                         int visibleItemCount, int totalItemCount) {
                        //leave this empty
                    }
                });

        new LoadScheduledOrders().execute();
        return rootView;
        }

        /**
         * Receiving push messages
         * */
        private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("yjfhv", "hjgchg");
                String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
                // Waking up mobile if it is sleeping
                WakeLocker.acquire(getActivity());
                newMessage="love is life";
                /**
                 * Take appropriate action on this message
                 * depending upon your app requirement
                 * For now i am just displaying it on the screen
                 * */

                // Showing received message
                Toast.makeText(getActivity(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
                // Releasing wake lock
                WakeLocker.release();
                new LoadScheduledOrders().execute();
            }
        };

        class LoadScheduledOrders extends AsyncTask<String, String, ArrayList<ScheduledOrdersSnippet>> {

            private ProgressDialog pDialog;
            private static final String url_scheduledOrders_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/scheduled_order_internal_lazylad";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Loading Orders. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();
            }

            @Override
            protected ArrayList<ScheduledOrdersSnippet> doInBackground(String... params) {
                String response_scheduledOrders_servProv;
                JSONObject json_data_scheduledOrders_servProv;
                JSONArray scheduledOrders_servProv;
                HTTPConnectionHandler hTTPConnection_scheduledOrders_ServProv = new HTTPConnectionHandler();
                ArrayList arrayList_scheduledOrders_ServProv = new ArrayList();

                String orderId;
                JSONObject jObject = null;

                if(m_scheduledOrdersList == null)
                {
                    orderId = "0";
                }
                else
                {

                    orderId = m_scheduledOrdersList.get(m_scheduledOrdersList.size() - 1).m_scheduledOrderCode;
                }

                SharedPreferences share_pref = getActivity().getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
                try {
                    jObject = new JSONObject(share_pref.getString("order_filter", "{}"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                arrayList_scheduledOrders_ServProv.add((Object)new BasicNameValuePair("before_order_code", orderId));
                arrayList_scheduledOrders_ServProv.add((Object)new BasicNameValuePair("order_filter", jObject.toString()));

                try{
                    response_scheduledOrders_servProv=hTTPConnection_scheduledOrders_ServProv.postResponse(url_scheduledOrders_serviceProvider, arrayList_scheduledOrders_ServProv);
                    Log.i("Response", response_scheduledOrders_servProv);
                    if(response_scheduledOrders_servProv==null){
                        return null;
                    }
                    else{
                        json_data_scheduledOrders_servProv=new JSONObject(response_scheduledOrders_servProv);
                        try{
                            int success = json_data_scheduledOrders_servProv.getInt("error");
                            if(success == 1) {
                                scheduledOrders_servProv = json_data_scheduledOrders_servProv.getJSONArray("scheduled_orders_service_providers");
                                int numberOfScheduledOrders = scheduledOrders_servProv.length();
                                ArrayList<ScheduledOrdersSnippet> scheduledOrdersList = new ArrayList<ScheduledOrdersSnippet>(numberOfScheduledOrders);

                                for (int i = 0; i < numberOfScheduledOrders; i++) {
                                    JSONObject scheduled_orders_list_object = scheduledOrders_servProv.getJSONObject(i);
                                    int scheduledOrderId = i + 1;
                                    String scheduledOrderCode = scheduled_orders_list_object.getString("scheduled_order_code");
                                    String address = scheduled_orders_list_object.getString("delivery_address");
                                    String userCode = Integer.toString(scheduled_orders_list_object.getInt("user_code"));
                                    String timeOrderPlaced = scheduled_orders_list_object.getString("order_time");
                                    Long timeOrderExpected = scheduled_orders_list_object.getLong("expected_del_time");
                                    int amount = scheduled_orders_list_object.getInt("total_amount");
                                    String userName = scheduled_orders_list_object.getString("user_name");
                                    String userPhoneNum = scheduled_orders_list_object.getString("user_phone_number");
                                    String orderStatus=scheduled_orders_list_object.getString("order_status");
                                    String spName=scheduled_orders_list_object.getString("sp_name");
                                    String coupon=scheduled_orders_list_object.getString("coupon_code");
                                    String verifiedNumber=scheduled_orders_list_object.getString("phone_number");
                                    int deliveryBoyRequested = scheduled_orders_list_object.getInt("rider_requested");
                                    int deliveryBoyAssigned = scheduled_orders_list_object.getInt("rider_assigned");
                                    int deliveryBoyConfirmed = scheduled_orders_list_object.getInt("rider_accepted");
                                    //  int deliveryBoyRequested = 1;
                                    // int deliveryBoyAssigned = 0;
                                    // int deliveryBoyConfirmed = 0;

                                    ScheduledOrdersSnippet schedOrderObj = new ScheduledOrdersSnippet(scheduledOrderId, scheduledOrderCode, address, userCode, timeOrderPlaced, timeOrderExpected, amount, userName, userPhoneNum, orderStatus, spName, coupon, verifiedNumber, deliveryBoyRequested, deliveryBoyAssigned, deliveryBoyConfirmed);
                                    scheduledOrdersList.add(schedOrderObj);
                                }
                                return scheduledOrdersList;
                            }else{
                                return null;
                            }

                        }
                        catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(ArrayList<ScheduledOrdersSnippet> scheduledOrdersList) {
                    if (null != scheduledOrdersList) {

                    if (m_scheduledOrdersList == null) {

                        m_scheduledOrdersList = scheduledOrdersList;
                        m_scheduledOrdersAdapter = new ScheduledOrdersAdapter(getActivity(), m_scheduledOrdersList);
                        m_scheduledOrdersDetails.setAdapter((ListAdapter) m_scheduledOrdersAdapter);
                    }
                    else
                    {
                        m_scheduledOrdersList.addAll(scheduledOrdersList);
                        m_scheduledOrdersAdapter.notifyDataSetChanged();
                    }
                }

                pDialog.dismiss();
                dataLoading = false;
            }
        }

    @Override
    public void onStart(){
        super.onStart();
  //      m_scheduledOrdersList = null;
//        new LoadScheduledOrders().execute();
    }
    }
