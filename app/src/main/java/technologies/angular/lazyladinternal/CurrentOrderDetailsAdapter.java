package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Saurabh on 30/01/15.
 */
public class CurrentOrderDetailsAdapter extends BaseAdapter {

    private ArrayList<CurrentOrderDetailsSnippet> m_currentOrderDetails;
    private Activity m_activity;

    private class ViewHolder {
        //TextView itemCodeTextView=(TextView)convertView.findViewById(R.id.item_code_textview);
        TextView itemNameTextView;
        //TextView itemShortDescTextView=(TextView)convertView.findViewById(R.id.item_short_desc_textview);
        TextView itemQuantityTextView;
        TextView itemCostTextView;
        TextView itemDescTextView;
        CheckBox itemConfirmedCheckBox;
        ImageView imageView;
    }

    public CurrentOrderDetailsAdapter(Activity activity, ArrayList<CurrentOrderDetailsSnippet> currentOrderDetails) {
        m_activity = activity;
        m_currentOrderDetails = currentOrderDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_currentOrderDetails != null) {
            count = m_currentOrderDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_currentOrderDetails != null) {
            return m_currentOrderDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_currentOrderDetails != null) {
            return m_currentOrderDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_currentOrderDetails != null) {
            Log.e("currentOrderDetails",m_currentOrderDetails.size()+"");
            final ViewHolder holder;

            String itemCode = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemCode;
            String itemName = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemName;
            String itemShortDesc = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemShortDesc;
            int itemQuantity = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemQuantity;
            double itemCost = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemCost;
            String itemDesc = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemDesc;
            String itemImageAdd = ((CurrentOrderDetailsSnippet) getItem(position)).m_itemImgAdd;

            final int pos = position;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.current_order_details_display_list, (ViewGroup) null);
                //TextView itemCodeTextView=(TextView)convertView.findViewById(R.id.item_code_textview);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                //TextView itemShortDescTextView=(TextView)convertView.findViewById(R.id.item_short_desc_textview);
                holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                holder.itemConfirmedCheckBox = (CheckBox) convertView.findViewById(R.id.item_confirmed_checbox);
                holder.imageView = (ImageView)convertView.findViewById(R.id.current_item_image);
                //holder.itemConfirmedCheckBox.setOnCheckedChangeListener(null);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            if(((CurrentOrderDetailsSnippet)getItem(position)).m_itemConfirmed == 1)
                holder.itemConfirmedCheckBox.setChecked(true);
            else
                holder.itemConfirmedCheckBox.setChecked(false);

            //itemCodeTextView.setText(itemCode);
            holder.itemNameTextView.setText(itemName);
            //itemShortDescTextView.setText(itemShortDesc);
            holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
            holder.itemCostTextView.setText(Double.toString(itemCost));
            holder.itemDescTextView.setText(itemDesc);

            if(itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.abc_btn_check_material)
                    .into(holder.imageView);


            Picasso.with(parent.getContext())
                    .setIndicatorsEnabled(true);


            holder.itemConfirmedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((CurrentOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 1;
                    } else {
                        ((CurrentOrderDetailsSnippet) getItem(pos)).m_itemConfirmed = 0;
                    }
                }
            });

        }
        return convertView;
    }
}
