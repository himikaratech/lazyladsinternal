package technologies.angular.lazyladinternal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by user on 06-08-2015.
 */
public class TabViewForFilter extends FragmentActivity {

    Button Tab1,Tab2;
    FragmentTabHost tabHost;
    public static String Tab="";
    private int mCurrentTabId;
    private SharedPreferences prefs;
    private JSONObject m_cityNames = new JSONObject();
    JSONObject merged = null;
    JSONObject applyData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_for_filter);

        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        Tab1=(Button)findViewById(R.id.Tab1);
        Tab2=(Button)findViewById(R.id.Tab2);

        Button apply=(Button)findViewById(R.id.apply_button);

        apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (mCurrentTabId == R.id.Tab1) {
                    Log.i("Fragment 1", "Ready to store data");
                    CitySelection cityFragment = (CitySelection)getSupportFragmentManager().findFragmentByTag(tabHost.getCurrentTabTag());
                    m_cityNames = cityFragment.getData();
                    applyData = m_cityNames;
                }
                else
                    applyData = new JSONObject();
                    addDataToMainJsonObject(applyData);

               prefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("order_filter", merged.toString());
                editor.commit();
                onBackPressed();

               }
        });

        tabHost.addTab(
                tabHost.newTabSpec("Tab1").setIndicator("Tab1", null),
                CitySelection.class, null);

        tabHost.addTab(
                tabHost.newTabSpec("Tab2").setIndicator("Tab2", null),
                SecondTab.class, null);
        mCurrentTabId = R.id.Tab1;
    }

    private void addDataToMainJsonObject(JSONObject receivedObj) {

            JSONObject obj = receivedObj;
            merged = new JSONObject();
            Log.i("received object", obj.toString());
            Iterator it = obj.keys();
            while (it.hasNext()) {
                String key = (String)it.next();
                try {
                    merged.put(key, obj.get(key));
                    Log.i("merged", merged.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    public void tabHandler(View target) {

        if (mCurrentTabId != target.getId()) {
            if (mCurrentTabId == R.id.Tab1) {
                Log.i("Fragment 1", "Ready to fetch data");
                CitySelection cityFragment = (CitySelection)getSupportFragmentManager().findFragmentByTag(tabHost.getCurrentTabTag());
                m_cityNames = cityFragment.getData();
                addDataToMainJsonObject(m_cityNames);
            }
            else if (mCurrentTabId == R.id.Tab2) {
                Tab = "this is Tab 2";
            }

            if (target.getId() == R.id.Tab1) {
                Tab = "this is Tab 1";
                tabHost.setCurrentTab(0);
            }
            else if (target.getId() == R.id.Tab2) {
                Tab = "this is Tab 2";
                tabHost.setCurrentTab(1);
            }

            mCurrentTabId = target.getId();
        }
    }
}
