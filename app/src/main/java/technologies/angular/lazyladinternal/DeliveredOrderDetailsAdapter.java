package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sakshigupta on 01/09/15.
 */
public class DeliveredOrderDetailsAdapter extends BaseAdapter {

    private ArrayList<DeliveredOrderDetailsSnippet> m_deliveredOrderDetails;
    private Activity m_activity;

    private class ViewHolder {
        TextView itemNameTextView;
        TextView itemQuantityTextView;
        TextView itemCostTextView;
        TextView itemDescTextView;
        ImageView imageView;
    }

    public DeliveredOrderDetailsAdapter(Activity activity, ArrayList<DeliveredOrderDetailsSnippet> deliveredOrderDetails) {
        m_activity = activity;
        m_deliveredOrderDetails = deliveredOrderDetails;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (m_deliveredOrderDetails != null) {
            count = m_deliveredOrderDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if (m_deliveredOrderDetails != null) {
            return m_deliveredOrderDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_deliveredOrderDetails != null) {
            return m_deliveredOrderDetails.get(position).m_itemId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (m_deliveredOrderDetails != null) {

            final ViewHolder holder;

            String itemCode = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemCode;
            String itemName = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemName;
            String itemShortDesc = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemShortDesc;
            int itemQuantity = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemQuantity;
            double itemCost = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemCost;
            String itemDesc = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemDesc;
            String itemImageAdd = ((DeliveredOrderDetailsSnippet) getItem(position)).m_itemImgAdd;

            final int pos = position;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.delivered_order_details_display_list, (ViewGroup) null);
                holder.itemNameTextView = (TextView) convertView.findViewById(R.id.item_name_textview);
                holder.itemQuantityTextView = (TextView) convertView.findViewById(R.id.item_quantity_textview);
                holder.itemCostTextView = (TextView) convertView.findViewById(R.id.item_cost_textview);
                holder.itemDescTextView = (TextView) convertView.findViewById(R.id.item_desc_textview);
                holder.imageView = (ImageView)convertView.findViewById(R.id.delivered_item_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.itemNameTextView.setText(itemName);
            holder.itemQuantityTextView.setText(Integer.toString(itemQuantity));
            holder.itemCostTextView.setText(Double.toString(itemCost));
            holder.itemDescTextView.setText(itemDesc);

            if(itemImageAdd == null || itemImageAdd == "") itemImageAdd = "\"\"";
            Picasso.with(parent.getContext())
                    .load(itemImageAdd)
                    .placeholder(R.drawable.abc_btn_check_material)
                    .into(holder.imageView);


            Picasso.with(parent.getContext())
                    .setIndicatorsEnabled(true);

        }
        return convertView;
    }

}
