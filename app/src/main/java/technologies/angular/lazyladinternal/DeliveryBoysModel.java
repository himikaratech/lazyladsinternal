package technologies.angular.lazyladinternal;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 12-08-2015.
 */
public class DeliveryBoysModel {

    public boolean success;
    public String city_code;

    public AssignedRider rider_assigned;

    public HashMap<String, ArrayList<DeliveryBoysSnippet> > riders_available;


}

class AssignedRider{
    public String rider_phone;
    public String rider_name;
}
