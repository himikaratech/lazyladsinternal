package technologies.angular.lazyladinternal;

/**
 * Created by Saurabh on 28/01/15.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {
    public static boolean isUserSignedIn(Context context) {
        return true;//SessionManager.getInstance(context).isLoggedIn();
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = (NetworkInfo)((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((networkInfo == null) || (!(networkInfo.isConnected()))) return false;
        return true;
    }

    public static String strForSqlite(String str){
        return str.replace("'","''");
    }
}