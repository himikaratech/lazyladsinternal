package technologies.angular.lazyladinternal;

/**
 * Created by Saurabh on 30/01/15.
 */
public class CurrentOrderDetailsSnippet {

    public int m_itemId;
    public String m_itemCode;
    public String m_itemName;
    public int m_itemImgFlag;
    public String m_itemImgAdd;
    public String m_itemShortDesc; //Weight like 20 gm wali dabi
    public int m_itemQuantity;
    public double m_itemCost;   //total cost i.e. cost*quantity
    public String m_itemDesc;

    public int m_itemConfirmed;   //0 for not available (by default); 1 for confirming

    public CurrentOrderDetailsSnippet(){

    }

    public CurrentOrderDetailsSnippet(int itemId, String itemCode, String itemName, int itemImgFlag, String itemImgAdd,
                                      String itemShortDesc, int itemQuantity, double itemCost, String itemDesc){
        m_itemId=itemId;
        m_itemCode=itemCode;
        m_itemName=itemName;
        m_itemImgFlag=itemImgFlag;
        m_itemImgAdd=itemImgAdd;
        m_itemShortDesc=itemShortDesc;
        m_itemQuantity=itemQuantity;
        m_itemCost=itemCost;
        m_itemDesc=itemDesc;
        m_itemConfirmed = 1;   // all items by default confirmed for internal app
    }

}
