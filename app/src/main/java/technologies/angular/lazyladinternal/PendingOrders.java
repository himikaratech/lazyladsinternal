package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PendingOrders extends ActionBarActivity {

    private Boolean dataLoading = false;
    Toolbar toolbar;

    private ArrayList<PendingOrdersSnippet> m_pendingOrdersList = null;
    private PendingOrdersAdapter m_pendingOrdersAdapter = null;
    private ListView m_pendingOrdersDetails;
    private MenuItem searchMenuItem;

    String API ="http://www.angulartechnologies.com/task_manager/v1";

    private AsyncTask m_async5;
    public static ContentResolver m_conRes=null;
    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;

    static{
        PendingOrders.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    static{
        PendingOrders.deleteUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
    }
    static{
        PendingOrders.insertUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
    }
    static{
        PendingOrders.updateUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startImplementation();
    }

    private void startImplementation(){
        m_conRes = getContentResolver();
        if(Utils.isNetworkAvailable(this)) {

            m_conRes = getContentResolver();
            setContentView(R.layout.pending_orders);

            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);

            m_pendingOrdersDetails = (ListView) findViewById(R.id.pending_orders_list_view);
            m_pendingOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                    if (null == m_pendingOrdersDetails)
                        return;

                    int threshold = 1;
                    int count = m_pendingOrdersDetails.getCount();

                    if (scrollState == SCROLL_STATE_IDLE) {
                        if (!dataLoading && m_pendingOrdersDetails.getLastVisiblePosition() >= count - threshold) {

                            if (Utils.isNetworkAvailable(PendingOrders.this)) {
                                dataLoading = true;
                                new LoadPendingOrders().execute();
                            } else {
                                Toast.makeText(PendingOrders.this, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    //leave this empty
                }

            });
            new LoadPendingOrders().execute();
        }
        else{
            setContentView(R.layout.network_problem);
            Button retryButton=(Button)findViewById(R.id.retry_button);

            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImplementation();
                }
            });
        }
        }

    class LoadPendingOrders extends AsyncTask<String, String, ArrayList<PendingOrdersSnippet>> {

        private ProgressDialog pDialog;
        private static final String url_pendingOrders_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/pending_order_internal_lazylad";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PendingOrders.this);
            pDialog.setMessage("Loading Orders. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected ArrayList<PendingOrdersSnippet> doInBackground(String... params) {
            String response_pendingOrders_servProv;
            JSONObject json_data_pendingOrders_servProv;
            JSONArray pendingOrders_servProv;
            HTTPConnectionHandler hTTPConnection_pendingOrders_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_pendingOrders_ServProv = new ArrayList();

            String orderId;
            JSONObject jObject = null;

            if (m_pendingOrdersList == null) {
                orderId = "0";
            } else {
                orderId = m_pendingOrdersList.get(m_pendingOrdersList.size() - 1).m_pendingOrderCode;
            }

            SharedPreferences share_pref = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
            try {
                jObject = new JSONObject(share_pref.getString("order_filter", "{}"));
                Log.i("send from pending", jObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            arrayList_pendingOrders_ServProv.add((Object) new BasicNameValuePair("before_order_code", orderId));
            arrayList_pendingOrders_ServProv.add((Object)new BasicNameValuePair("order_filter", jObject.toString()));

            try {
                response_pendingOrders_servProv = hTTPConnection_pendingOrders_ServProv.postResponse(url_pendingOrders_serviceProvider, arrayList_pendingOrders_ServProv);
                Log.i("Response Pending", response_pendingOrders_servProv);
                if (response_pendingOrders_servProv == null) {
                    return null;
                } else {
                    json_data_pendingOrders_servProv = new JSONObject(response_pendingOrders_servProv);
                    try {
                    int success = json_data_pendingOrders_servProv.getInt("error");
                        Log.i("Success", success+"");
                        if (success == 1) {
                            pendingOrders_servProv = json_data_pendingOrders_servProv.getJSONArray("pending_orders_service_providers");
                            int numberOfPendingOrders = pendingOrders_servProv.length();
                            ArrayList<PendingOrdersSnippet> pendingOrdersList = new ArrayList<PendingOrdersSnippet>(numberOfPendingOrders);

                            for (int i = 0; i < numberOfPendingOrders; i++) {
                                JSONObject pending_orders_list_object = pendingOrders_servProv.getJSONObject(i);
                                int pendingOrderId = i + 1;
                                String pendingOrderCode = pending_orders_list_object.getString("pending_order_code");
                                String address = pending_orders_list_object.getString("delivery_address");
                                String user_code = Integer.toString(pending_orders_list_object.getInt("user_code"));
                                String timeOrderPlaced = pending_orders_list_object.getString("order_time");
                                int amount = pending_orders_list_object.getInt("total_amount");
                                String userName = pending_orders_list_object.getString("user_name");
                                String userPhoneNum = pending_orders_list_object.getString("user_phone_number");
                                String coupon = pending_orders_list_object.getString("coupon_code");
                                int deliveryBoyRequested = pending_orders_list_object.getInt("rider_requested");
                                int deliveryBoyAssigned = pending_orders_list_object.getInt("rider_assigned");
                                int deliveryBoyConfirmed = pending_orders_list_object.getInt("rider_accepted");
                              //  int deliveryBoyRequested = 1;
                               // int deliveryBoyAssigned = 0;
                               // int deliveryBoyConfirmed = 0;
                                Log.i("username", userName);
                                PendingOrdersSnippet pendingOrderObj = new PendingOrdersSnippet(pendingOrderId, pendingOrderCode, address, user_code, timeOrderPlaced, amount, userName, userPhoneNum, coupon, deliveryBoyRequested, deliveryBoyAssigned, deliveryBoyConfirmed);
                                pendingOrdersList.add(pendingOrderObj);
                            }
                            return pendingOrdersList;
                        } else {
                            return null;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(ArrayList<PendingOrdersSnippet> pendingOrdersList) {

                if (null != pendingOrdersList) {
                if (m_pendingOrdersList == null) {

                    m_pendingOrdersList = pendingOrdersList;
                    m_pendingOrdersAdapter = new PendingOrdersAdapter(PendingOrders.this, m_pendingOrdersList);
                    m_pendingOrdersDetails.setAdapter((ListAdapter) m_pendingOrdersAdapter);
                } else {
                    m_pendingOrdersList.addAll(pendingOrdersList);
                    m_pendingOrdersAdapter.notifyDataSetChanged();
                }
            }
            pDialog.dismiss();
            dataLoading = false;
        }
    }

 //   @Override
 //   protected void onRestart() {
  //      Log.i("Restart", "restart");
   //     super.onRestart();
      //  m_pendingOrdersList = null;
       // new LoadPendingOrders().execute();
   // }


    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_results, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MenuItem searchMenuItem = getSearchMenuItem();
                if (searchMenuItem != null) {
                    SearchView sv = (SearchView) searchMenuItem.getActionView();
                    sv.onActionViewCollapsed();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("Delete db", "Delete");
                PendingOrders.m_conRes.query(PendingOrders.deleteUri, PendingOrders.dummy, "delete from search_result", PendingOrders.dummy, "");
                if (s.length() > 2) {
                    Log.i("location", "TextChange");
                    loadData(s);
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    private void loadData(final String searchText) {
        JSONObject jObject = null;
        //   String sendToServer[];
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        SearchApi searchService = restAdapter.create(SearchApi.class);
        Log.i("search_text", searchText);
        SharedPreferences sprefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
        try {
            jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Jobject_search_order", jObject.toString());
        SearchApi.InputForSearchOrders search = new SearchApi.InputForSearchOrders(searchText, "0", jObject.toString());
        searchService.searchFeeds(search, new Callback<SearchApi.SearchModel>() {
            @Override
            public void success(SearchApi.SearchModel orderResults, Response response) {
                int success = orderResults.error;
                if (success == 1) {

                    ArrayList<SearchResultSnippet> orderList = orderResults.orders_service_providers;

                    Log.e("sucess tabViewForOrders", orderResults.error + "");
                    m_async5 = new StoreItemsInSqllite().execute(orderList);

                } else {
                    Toast.makeText(getApplicationContext(), "No result Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
            }
        });
    }

    private class StoreItemsInSqllite extends AsyncTask<ArrayList, Void, Void> {
        @Override
        protected Void doInBackground(ArrayList... params) {
            ArrayList<SearchResultSnippet> orderList = params[0];
            if(orderList == null)
                return null;

            for (int i = 0; i < orderList.size(); i++) {
                if (isCancelled()) {
                    break;
                }

                Log.i("search_result ", "working");

                int ordersId = i+1;
                String orderCode = orderList.get(i).m_orderCode;
                String address = Utils.strForSqlite(orderList.get(i).m_address);
                String userCode = orderList.get(i).m_userCode;
                String timeOrderPlaced = orderList.get(i).m_timeOrderPlaced;
                Long expectedTime = orderList.get(i).m_expectedTime;
                int amount = orderList.get(i).m_amount;
                String userName = Utils.strForSqlite(orderList.get(i).m_userName);
                String userPhoneNum = orderList.get(i).m_userPhoneNum;
                String orderStatus = orderList.get(i).m_orderStatus;
                String spName = Utils.strForSqlite(orderList.get(i).m_spName);
                Long sellerDelTime = orderList.get(i).m_sellerTime;

                Log.i("data for order status", orderStatus);
                m_conRes.query(PendingOrders.insertUri, dummy, "insert into search_result (order_id, order_code, order_address, " +
                        "user_code, time_order_placed, expected_time, amount, user_name, user_phone, order_status, " +
                        "sp_name, seller_time) values ('" + ordersId + "', '" + orderCode + "', '" + address + "', '" + userCode + "', " +
                        "'" + timeOrderPlaced + "', '" + expectedTime + "', '" + amount + "', '" + userName + "', '" + userPhoneNum + "', " +
                        "'" + orderStatus + "', '" + spName + "', '" + sellerDelTime + "')", dummy, "");
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
}
