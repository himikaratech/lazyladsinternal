package technologies.angular.lazyladinternal;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DeliveredOrders extends ActionBarActivity {

    Toolbar toolbar;
    private Boolean dataLoading = false;
    private ArrayList<DeliveredOrdersSnippet> m_deliveredOrdersList = null;
    private DeliveredOrdersAdapter m_deliveredOrdersAdapter = null;
    private ListView m_deliveredOrdersDetails;
    private MenuItem searchMenuItem;
    String API ="http://www.angulartechnologies.com/task_manager/v1";

    private AsyncTask m_async5;
    public static ContentResolver m_conRes=null;
    public static String[] dummy = new String[]{"a", "b"};
    public static Uri deleteUri;
    public static Uri insertUri;
    public static Uri selectUri;
    public static Uri updateUri;

    static{
        DeliveredOrders.selectUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
    }

    static{
        DeliveredOrders.deleteUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
    }
    static{
        DeliveredOrders.insertUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
    }
    static{
        DeliveredOrders.updateUri = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startImplementation();
    }

    private void startImplementation(){
        if(Utils.isNetworkAvailable(this)){
            setContentView(R.layout.delivered_orders);

            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            m_conRes = this.getContentResolver();
            ImageButton filter =(ImageButton)findViewById(R.id.button_filter);
            filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(DeliveredOrders.this, TabViewForFilter.class);
                    startActivity(in);
                }
            });

            m_deliveredOrdersDetails = (ListView)findViewById(R.id.delivered_orders_list_view);
            m_deliveredOrdersDetails.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) { // TODO Auto-generated method stub
                    if (null == m_deliveredOrdersDetails)
                        return;

                    int threshold = 1;
                    int count = m_deliveredOrdersDetails.getCount();

                    if (scrollState == SCROLL_STATE_IDLE) {
                        if (!dataLoading && m_deliveredOrdersDetails.getLastVisiblePosition() >= count - threshold) {
                            if (Utils.isNetworkAvailable(DeliveredOrders.this)) {
                                dataLoading = true;
                                // Execute LoadMoreDataTask AsyncTask
                                new LoadDeliveredOrders().execute();
                            } else {
                                Toast.makeText(DeliveredOrders.this, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    //leave this empty
                }
            });
            new LoadDeliveredOrders().execute();
        }else{
            setContentView(R.layout.network_problem);
            Button retryButton=(Button)findViewById(R.id.retry_button);

            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImplementation();
                }
            });
        }
    }


    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delivered_orders, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MenuItem searchMenuItem = getSearchMenuItem();
                if (searchMenuItem != null) {
                    SearchView sv = (SearchView) searchMenuItem.getActionView();
                    sv.onActionViewCollapsed();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("Delete db", "Delete");
                DeliveredOrders.m_conRes.query(DeliveredOrders.deleteUri, DeliveredOrders.dummy, "delete from search_result", DeliveredOrders.dummy, "");
                if (s.length() > 2) {
                    Log.i("location", "TextChange");
                    loadData(s);
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    private void loadData(final String searchText) {
        JSONObject jObject = null;
        //   String sendToServer[];
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();
        SearchApi searchService = restAdapter.create(SearchApi.class);
        Log.i("search_text", searchText);
        SharedPreferences sprefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
        try {
            jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Jobject_search_order", jObject.toString());
        SearchApi.InputForSearchOrders search = new SearchApi.InputForSearchOrders(searchText, "0", jObject.toString());
        searchService.searchFeeds(search, new Callback<SearchApi.SearchModel>() {
            @Override
            public void success(SearchApi.SearchModel orderResults, Response response) {
                int success = orderResults.error;
                if (success == 1) {

                    ArrayList<SearchResultSnippet> orderList = orderResults.orders_service_providers;

                    Log.e("sucess tabViewForOrders", orderResults.error + "");
                    m_async5 = new StoreItemsInSqllite().execute(orderList);

                } else {
                    Toast.makeText(getApplicationContext(), "No result Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("", error.toString());
            }
        });
    }

    private class StoreItemsInSqllite extends AsyncTask<ArrayList, Void, Void> {
        @Override
        protected Void doInBackground(ArrayList... params) {
            ArrayList<SearchResultSnippet> orderList = params[0];
            if(orderList == null)
                return null;

            for (int i = 0; i < orderList.size(); i++) {
                if (isCancelled()) {
                    break;
                }

                Log.i("search_result ", "working");

                int ordersId = i+1;
                String orderCode = orderList.get(i).m_orderCode;
                String address = Utils.strForSqlite(orderList.get(i).m_address);
                String userCode = orderList.get(i).m_userCode;
                String timeOrderPlaced = orderList.get(i).m_timeOrderPlaced;
                Long expectedTime = orderList.get(i).m_expectedTime;
                int amount = orderList.get(i).m_amount;
                String userName = Utils.strForSqlite(orderList.get(i).m_userName);
                String userPhoneNum = orderList.get(i).m_userPhoneNum;
                String orderStatus = orderList.get(i).m_orderStatus;
                String spName = Utils.strForSqlite(orderList.get(i).m_spName);
                Long sellerDelTime = orderList.get(i).m_sellerTime;

                Log.i("data for order status", orderStatus);
                m_conRes.query(TabViewForOrders.insertUri, dummy, "insert into search_result (order_id, order_code, order_address, " +
                        "user_code, time_order_placed, expected_time, amount, user_name, user_phone, order_status, " +
                        "sp_name, seller_time) values ('" + ordersId + "', '" + orderCode + "', '" + address + "', '" + userCode + "', " +
                        "'" + timeOrderPlaced + "', '" + expectedTime + "', '" + amount + "', '" + userName + "', '" + userPhoneNum + "', " +
                        "'" + orderStatus + "', '" + spName + "', '" + sellerDelTime + "')", dummy, "");
            }
            return null;
        }
    }
    class LoadDeliveredOrders extends AsyncTask<String, String, ArrayList<DeliveredOrdersSnippet>> {

        private ProgressDialog pDialog;
        private static final String url_deliveredOrders_serviceProvider = "http://www.angulartechnologies.com/task_manager/v1/delivered_order_internal_lazylad";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DeliveredOrders.this);
            pDialog.setMessage("Loading Delivered Orders. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected ArrayList<DeliveredOrdersSnippet> doInBackground(String... params) {
            String response_deliveredOrders_servProv;
            JSONObject json_data_deliveredOrders_servProv;
            JSONArray deliveredOrders_servProv;
            HTTPConnectionHandler hTTPConnection_deliveredOrders_ServProv = new HTTPConnectionHandler();
            ArrayList arrayList_deliveredOrders_ServProv = new ArrayList();

            String orderId;
            JSONObject jObject = null;
            if(m_deliveredOrdersList == null) {
                orderId = "0";
            }
            else {

                orderId = m_deliveredOrdersList.get(m_deliveredOrdersList.size() - 1).orderCode;
            }
            SharedPreferences sprefs = getSharedPreferences("technologies.angular.lazyladinternal", Context.MODE_PRIVATE);
            try {
                jObject = new JSONObject(sprefs.getString("order_filter", "{}"));
                Log.i("send from delivered", jObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            arrayList_deliveredOrders_ServProv.add((Object)new BasicNameValuePair("before_order_code", orderId));
            arrayList_deliveredOrders_ServProv.add((Object)new BasicNameValuePair("order_filter", jObject.toString()));

            try{
                response_deliveredOrders_servProv=hTTPConnection_deliveredOrders_ServProv.postResponse(url_deliveredOrders_serviceProvider, arrayList_deliveredOrders_ServProv);
                Log.i("Response", response_deliveredOrders_servProv);
                if(response_deliveredOrders_servProv==null){
                    return null;
                }
                else{
                    json_data_deliveredOrders_servProv=new JSONObject(response_deliveredOrders_servProv);
                    try{
                        int success = json_data_deliveredOrders_servProv.getInt("error");
                        if(success == 1){
                            deliveredOrders_servProv=json_data_deliveredOrders_servProv.getJSONArray("delivered_orders_service_providers");
                            int numberOfDeliveredOrders=deliveredOrders_servProv.length();
                            ArrayList<DeliveredOrdersSnippet> deliveredOrdersList = new ArrayList<DeliveredOrdersSnippet>(numberOfDeliveredOrders);

                            for(int i=0; i<numberOfDeliveredOrders; i++){
                                JSONObject delivered_orders_list_object = deliveredOrders_servProv.getJSONObject(i);
                                int deliveredOrderId=i+1;
                                String order_code=delivered_orders_list_object.getString("delivered_order_code");
                                String address=delivered_orders_list_object.getString("delivery_address");
                                String timeOrderPlaced=delivered_orders_list_object.getString("order_time");
                                double amount=delivered_orders_list_object.getDouble("total_amount");
                                String userName=delivered_orders_list_object.getString("user_name");
                                String userPhoneNum=delivered_orders_list_object.getString("user_phone_number");
                                Log.i("name", userName);
                                String coupon=delivered_orders_list_object.getString("coupon_code");

                                DeliveredOrdersSnippet delOrderObj = new DeliveredOrdersSnippet(deliveredOrderId, order_code, address, timeOrderPlaced, amount, userName, userPhoneNum, coupon);
                                deliveredOrdersList.add(delOrderObj);
                            }
                            return deliveredOrdersList;
                        }else{
                            return null;
                        }
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(ArrayList<DeliveredOrdersSnippet> deliveredOrdersList) {

               if (null != deliveredOrdersList) {
                if (m_deliveredOrdersList == null) {
                    Log.i("list size", deliveredOrdersList+"");
                    m_deliveredOrdersList = deliveredOrdersList;
                    Log.i("list size1", deliveredOrdersList+"");
                    m_deliveredOrdersAdapter = new DeliveredOrdersAdapter(DeliveredOrders.this, m_deliveredOrdersList);
                    Log.i("list size1", deliveredOrdersList+"");
                    m_deliveredOrdersDetails.setAdapter((ListAdapter) m_deliveredOrdersAdapter);
                }
                else
                {
                    m_deliveredOrdersList.addAll(deliveredOrdersList);
                    m_deliveredOrdersAdapter.notifyDataSetChanged();
                }
            }
            pDialog.dismiss();
            dataLoading = false;
        }
    }

  /*  @Override
    protected void onRestart() {
        Log.i("Restart", "restart");
        super.onRestart();
        m_deliveredOrdersList = null;
        new LoadDeliveredOrders().execute();
    }*/
}
