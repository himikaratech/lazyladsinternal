package technologies.angular.lazyladinternal;

/**
 * Created by Saurabh on 26/02/15.
 */
public class PendingOrdersSnippet {

    public int pendingOrdersId;
    public String m_pendingOrderCode;
    public String m_address;
    public String m_userCode;
    public String m_timeOrderPlaced;
    public int m_amount;
    public String m_userName;
    public String m_userPhoneNum;
    public String m_orderStatus;
    public String m_coupon;
    public int m_delBoyRequested;
    public int m_delBoyAssigned;
    public int m_delBoyConfirmed;

    public PendingOrdersSnippet(){
    }

    public PendingOrdersSnippet(int id, String pendingOrderCode, String customer_address, String user_code, String time_order_placed, int total_amount, String userName, String userPhoneNum, String coupon, int delBoyRequested, int delBoyAssigned, int delBoyConfirmed){
        pendingOrdersId=id;
        m_pendingOrderCode=pendingOrderCode;
        m_address=customer_address;
        m_userCode=user_code;
        m_timeOrderPlaced=time_order_placed;
        m_amount=total_amount;
        m_userName=userName;
        m_userPhoneNum=userPhoneNum;
        m_orderStatus="Confirmed";
        m_coupon = coupon;
        m_delBoyRequested = delBoyRequested;
        m_delBoyAssigned = delBoyAssigned;
        m_delBoyConfirmed = delBoyConfirmed;
    }
}
