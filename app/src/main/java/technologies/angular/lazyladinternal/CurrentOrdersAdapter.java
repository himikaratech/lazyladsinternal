package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

/**
 * Created by Saurabh on 30/01/15.
 */
public class CurrentOrdersAdapter extends BaseAdapter {


    private class ViewHolder {

      //  Button cancel_order_button;
        Button delivery_boy;
     //   TextView orderCancelled;
        ImageView requestedFlagImage;
        ImageView assignedFlagImage;
        ImageView confirmedFlagImage;
        int riderFlag = 0;
    }

    private ArrayList<CurrentOrdersSnippet> m_currentOrdersDetails;
    private Activity m_activity;


    public CurrentOrdersAdapter(Activity activity, ArrayList<CurrentOrdersSnippet> currentOrdersDetails){
        m_activity=activity;
        m_currentOrdersDetails=currentOrdersDetails;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_currentOrdersDetails != null){
            count=m_currentOrdersDetails.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_currentOrdersDetails != null){
            return m_currentOrdersDetails.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_currentOrdersDetails != null){
            return m_currentOrdersDetails.get(position).currentOrdersId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(m_currentOrdersDetails!=null){

           final ViewHolder holder;
            final int positionFinal = position;
           final String currentOrderCode=((CurrentOrdersSnippet) getItem(position)).m_currentOrderCode;
            String currentOrderAddress=((CurrentOrdersSnippet) getItem(position)).m_address;
            int currentOrderAmount=((CurrentOrdersSnippet) getItem(position)).m_amount;
            String currentOrderTimePlaced=((CurrentOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            String currentOrderUserName = ((CurrentOrdersSnippet)getItem(position)).m_userName;
            String currentOrderPhoneNumber = ((CurrentOrdersSnippet)getItem(position)).m_userPhoneNum;
            String orderStatus = ((CurrentOrdersSnippet) getItem(position)).m_orderStatus;
            String currentOrderCoupon = ((CurrentOrdersSnippet) getItem(position)).m_coupon;
            String currentOrderShopName = ((CurrentOrdersSnippet) getItem(position)).m_spName;
            String currentOrderVerifiedNumber = ((CurrentOrdersSnippet) getItem(position)).m_verifiedNumber;

            final String userCode=((CurrentOrdersSnippet) getItem(position)).m_userCode;
            int deliveryBoyRequested = ((CurrentOrdersSnippet) getItem(position)).m_delBoyRequested;
            int deliveryBoyAssigned = ((CurrentOrdersSnippet) getItem(position)).m_delBoyAssigned;
            int deliveryBoyConfirmed = ((CurrentOrdersSnippet) getItem(position)).m_delBoyAssigned;

            if(convertView == null){
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.current_orders_display_list, (ViewGroup)null);
            //    holder.cancel_order_button = (Button) convertView.findViewById(R.id.cancel_prev_order_button);
              //  holder.orderCancelled = (TextView) convertView.findViewById(R.id.order_cancelled);
                holder.delivery_boy = (Button) convertView.findViewById(R.id.deliveryBoy);
                holder.requestedFlagImage = (ImageView) convertView.findViewById(R.id.requestedFlagImage);
                holder.assignedFlagImage = (ImageView) convertView.findViewById(R.id.assignedFlagImage);
                holder.confirmedFlagImage = (ImageView) convertView.findViewById(R.id.confirmedFlagImage);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder)convertView.getTag();
            }

            TextView currentUserCodeTextView=(TextView)convertView.findViewById(R.id.current_user_code_textview);
            TextView currentOrderCodeTextView=(TextView)convertView.findViewById(R.id.current_order_code_textview);
            TextView currentOrderAddressTextView=(TextView)convertView.findViewById(R.id.current_order_address_textview);
            TextView currentOrderAmountTextView=(TextView)convertView.findViewById(R.id.amount_current_order_textview);
            TextView currentOrderTimePlacedTextView=(TextView)convertView.findViewById(R.id.current_order_del_textview);
            TextView currentOrderUserNameTextView =(TextView)convertView.findViewById(R.id.current_order_username_textview);
            TextView currentOrderCouponTextView =(TextView)convertView.findViewById(R.id.current_coupon_code_textview);
            TextView currentOrderUserPhoneTextView =(TextView)convertView.findViewById(R.id.current_order_number_textview);
            TextView currentOrderShopNameTextView =(TextView)convertView.findViewById(R.id.current_order_shopname_textview);
            TextView currentVerifiedNumberTextView=(TextView)convertView.findViewById(R.id.current_verified_number_textview);
            LinearLayout header =(LinearLayout)convertView.findViewById(R.id.head_order);
          //  holder.orderCancelled.setText("");
           // holder.cancel_order_button.setVisibility(View.VISIBLE);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            header.setBackgroundColor(color);

            if (((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1) {
                holder.riderFlag = 1;
            }
            else {
                holder.riderFlag = 0;
            }


            if (((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1) {
                if(((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1){
                    holder.delivery_boy.setVisibility(View.VISIBLE);
                }
                else
                    holder.delivery_boy.setVisibility(View.VISIBLE);
            }
            else
                holder.delivery_boy.setVisibility(View.INVISIBLE);


            currentUserCodeTextView.setText(userCode);
            currentOrderCodeTextView.setText(currentOrderCode);
            currentOrderAddressTextView.setText(currentOrderAddress);
            currentOrderAmountTextView.setText("₹ " + Integer.toString(currentOrderAmount));
            currentOrderTimePlacedTextView.setText(currentOrderTimePlaced);
            currentOrderUserNameTextView.setText(currentOrderUserName);
            currentOrderUserPhoneTextView.setText(currentOrderPhoneNumber);
            currentOrderShopNameTextView.setText(currentOrderShopName);
            currentOrderCouponTextView.setText(currentOrderCoupon);
            currentVerifiedNumberTextView.setText(currentOrderVerifiedNumber);

            if (((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1)
                holder.requestedFlagImage.setImageResource(R.drawable.active);
            else
                holder.requestedFlagImage.setImageResource(R.drawable.inactive);

            if(((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyAssigned == 1)
                holder.assignedFlagImage.setImageResource(R.drawable.active);
            else
                holder.assignedFlagImage.setImageResource(R.drawable.inactive);

            if(((CurrentOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1)
                holder.confirmedFlagImage.setImageResource(R.drawable.active);
            else
                holder.confirmedFlagImage.setImageResource(R.drawable.inactive);


         /*   if (orderStatus.equals("Cancelled")) {
                holder.orderCancelled.setText("Cancelled");
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }
*/
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCurrentOrderDetilsActivity(currentOrderCode, userCode);
                }
            });

            holder.delivery_boy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent (m_activity, AssignOrChangeDeliveryBoy.class);
                    String order_code = currentOrderCode;
                    in.putExtra("order_code", order_code);
                    in.putExtra("riderFlag", holder.riderFlag);
                    Log.i("rider flag", holder.riderFlag+"");
                    m_activity.startActivity(in);
                }
            });

          /*  holder.cancel_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelThisOrder(currentOrderCode);
                    ((CurrentOrdersSnippet) getItem(positionFinal)).m_orderStatus = "Cancelled";
                    holder.orderCancelled.setText("Cancelled");
                    holder.cancel_order_button.setVisibility(View.INVISIBLE);
                }
            });
            */
        }
        return convertView;
    }

    private void startCurrentOrderDetilsActivity(String currentOrderCode, String userCode){
        Intent intent = new Intent(m_activity, CurrentOrderDetails.class);
        String data[] = {currentOrderCode, userCode};
        intent.putExtra("data_send", data);
        m_activity.startActivity(intent);
    }

    private void cancelThisOrder(String order_code){
        String url_cancel_order = "http://www.angulartechnologies.com/task_manager/v1/cancelUsersPreviousOrder";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_code", order_code);

        JsonObjectRequest sendOrderDetails = new JsonObjectRequest(url_cancel_order, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("error");
                            if (success == 1) {

                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(m_activity);
        m_sessionManager.addToRequestQueue(sendOrderDetails);

    }
 }
