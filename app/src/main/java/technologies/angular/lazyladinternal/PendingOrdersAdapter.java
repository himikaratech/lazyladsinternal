package technologies.angular.lazyladinternal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Saurabh on 26/02/15.
 */
public class PendingOrdersAdapter extends BaseAdapter{

    private class ViewHolder {
      //  Button cancel_order_button;
        Button delivery_boy;
    //    TextView orderCancelled;
        ImageView requestedFlagImage;
        ImageView assignedFlagImage;
        ImageView confirmedFlagImage;
        int riderFlag = 0;
    }
    private Activity m_activity;
    private ArrayList<PendingOrdersSnippet> m_pendingOrdersSnippetArrayList;

    public PendingOrdersAdapter(Activity activity, ArrayList<PendingOrdersSnippet> pendingOrdersSnippetArrayList){
        m_activity=activity;
        m_pendingOrdersSnippetArrayList=pendingOrdersSnippetArrayList;
    }

    @Override
    public int getCount() {
        int count=0;
        if(m_pendingOrdersSnippetArrayList != null){
            count=m_pendingOrdersSnippetArrayList.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        if(m_pendingOrdersSnippetArrayList != null){
            return m_pendingOrdersSnippetArrayList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(m_pendingOrdersSnippetArrayList != null){
            return m_pendingOrdersSnippetArrayList.get(position).pendingOrdersId;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(m_pendingOrdersSnippetArrayList!=null){

            final ViewHolder holder;
            final int positionFinal = position;
            final String pendingOrderCode=((PendingOrdersSnippet) getItem(position)).m_pendingOrderCode;
            String pendingOrderAddress=((PendingOrdersSnippet) getItem(position)).m_address;
            int pendingOrderAmount=((PendingOrdersSnippet) getItem(position)).m_amount;
            String pendingOrderTimePlaced=((PendingOrdersSnippet) getItem(position)).m_timeOrderPlaced;
            String pendingOrderUserName = ((PendingOrdersSnippet)getItem(position)).m_userName;
            String pendingOrderPhoneNumber = ((PendingOrdersSnippet)getItem(position)).m_userPhoneNum;
            String orderStatus = ((PendingOrdersSnippet) getItem(position)).m_orderStatus;
            String pendingOrderCoupon = ((PendingOrdersSnippet) getItem(position)).m_coupon;
            final String userCode=((PendingOrdersSnippet) getItem(position)).m_userCode;
            int deliveryBoyRequested = ((PendingOrdersSnippet) getItem(position)).m_delBoyRequested;
            int deliveryBoyAssigned = ((PendingOrdersSnippet) getItem(position)).m_delBoyAssigned;
            int deliveryBoyConfirmed = ((PendingOrdersSnippet) getItem(position)).m_delBoyAssigned;

            if(convertView == null){
                holder = new ViewHolder();
                LayoutInflater infalInflater = (LayoutInflater) m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.pending_orders_display_list, (ViewGroup)null);
           //     holder.cancel_order_button = (Button) convertView.findViewById(R.id.cancel_prev_order_button);
            //    holder.orderCancelled = (TextView) convertView.findViewById(R.id.order_cancelled);
                holder.delivery_boy = (Button) convertView.findViewById(R.id.deliveryBoy);
                holder.requestedFlagImage = (ImageView) convertView.findViewById(R.id.requestedFlagImage);
                holder.assignedFlagImage = (ImageView) convertView.findViewById(R.id.assignedFlagImage);
                holder.confirmedFlagImage = (ImageView) convertView.findViewById(R.id.confirmedFlagImage);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder)convertView.getTag();
            }

            TextView pendingUserCodeTextView=(TextView)convertView.findViewById(R.id.pending_user_code_textview);
            TextView pendingOrderCodeTextView=(TextView)convertView.findViewById(R.id.pending_order_code_textview);
            TextView pendingOrderAddressTextView=(TextView)convertView.findViewById(R.id.pending_order_address_textview);
            TextView pendingOrderAmountTextView=(TextView)convertView.findViewById(R.id.amount_pending_order_textview);
            TextView pendingOrderTimePlacedTextView=(TextView)convertView.findViewById(R.id.pending_order_del_textview);
            TextView pendingOrderUserNameTextView =(TextView)convertView.findViewById(R.id.pending_order_username_textview);
            TextView pendingOrderCouponTextView =(TextView)convertView.findViewById(R.id.pending_coupon_code_textview);
            TextView pendingOrderUserPhoneTextView =(TextView)convertView.findViewById(R.id.pending_order_number_textview);
            LinearLayout header =(LinearLayout)convertView.findViewById(R.id.head_order);
          //  holder.orderCancelled.setText("");
          //  holder.cancel_order_button.setVisibility(View.VISIBLE);

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            header.setBackgroundColor(color);

            if (((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1) {
                holder.riderFlag = 1;
            }
            else {
                holder.riderFlag = 0;
            }


            if (((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1) {
                if(((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1){
                    holder.delivery_boy.setVisibility(View.VISIBLE);
                }
                else
                    holder.delivery_boy.setVisibility(View.VISIBLE);
            }
            else
                holder.delivery_boy.setVisibility(View.INVISIBLE);

            pendingUserCodeTextView.setText(userCode);
            pendingOrderCodeTextView.setText(pendingOrderCode);
            pendingOrderAddressTextView.setText(pendingOrderAddress);
            pendingOrderAmountTextView.setText("₹ " + Integer.toString(pendingOrderAmount));
            pendingOrderTimePlacedTextView.setText(pendingOrderTimePlaced);
            pendingOrderUserNameTextView.setText(pendingOrderUserName);
            pendingOrderUserPhoneTextView.setText(pendingOrderPhoneNumber);
            pendingOrderCouponTextView.setText(pendingOrderCoupon);

            if (((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyRequested == 1)
                holder.requestedFlagImage.setImageResource(R.drawable.active);
            else
                holder.requestedFlagImage.setImageResource(R.drawable.inactive);

            if(((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyAssigned == 1)
                holder.assignedFlagImage.setImageResource(R.drawable.active);
            else
                holder.assignedFlagImage.setImageResource(R.drawable.inactive);

            if(((PendingOrdersSnippet) getItem(positionFinal)).m_delBoyConfirmed == 1)
                holder.confirmedFlagImage.setImageResource(R.drawable.active);
            else
                holder.confirmedFlagImage.setImageResource(R.drawable.inactive);


         /*   if (orderStatus.equals("Cancelled")) {
                holder.orderCancelled.setText("Cancelled");
                holder.cancel_order_button.setVisibility(View.INVISIBLE);
            }
*/

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startPendingOrderDetilsActivity(pendingOrderCode, userCode);
                }
            });

            holder.delivery_boy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Intent in = new Intent (m_activity, AssignOrChangeDeliveryBoy.class);
                    String order_code = pendingOrderCode;
                    in.putExtra("order_code", order_code);
                    in.putExtra("riderFlag", holder.riderFlag);
                    Log.i("rider flag", holder.riderFlag + "");
                    m_activity.startActivity(in);
                }
            });

     /*       holder.cancel_order_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelThisOrder(pendingOrderCode);
                    ((PendingOrdersSnippet) getItem(positionFinal)).m_orderStatus = "Cancelled";
                    holder.orderCancelled.setText("Cancelled");
                    holder.cancel_order_button.setVisibility(View.INVISIBLE);
                }
            });
            */
        }
        return convertView;
    }

    private void startPendingOrderDetilsActivity(String pendingOrderCode, String userCode){
        Intent intent = new Intent(m_activity, PendingOrderDetails.class);
        String data[] = {pendingOrderCode, userCode};
        intent.putExtra("data_send_pending_details", data);
        m_activity.startActivity(intent);
    }

    private void cancelThisOrder(String order_code){
        String url_cancel_order = "http://www.angulartechnologies.com/task_manager/v1/cancelUsersPreviousOrder";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("order_code", order_code);

        JsonObjectRequest sendOrderDetails = new JsonObjectRequest(url_cancel_order, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("error");
                            if (success == 1) {

                            } else {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        SessionManager m_sessionManager;
        m_sessionManager = new SessionManager(m_activity);
        m_sessionManager.addToRequestQueue(sendOrderDetails);

    }
}
