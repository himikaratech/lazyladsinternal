package technologies.angular.lazyladinternal;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

/**
 * Created by Saurabh on 27/01/15.
 */
public class SessionManager extends Application {

    private Context m_context;
    SharedPreferences.Editor m_editor = null;
    private SharedPreferences m_pref;
    private int PRIVATE_MODE=0;
    private static SessionManager m_sessMgr;

    public static final String TAG = "VolleyPatterns";
    private RequestQueue mRequestQueue;
    public static final String USER_ID="USER_ID";


    public SessionManager(Context context){
        this.m_context = context;
        m_pref = m_context.getSharedPreferences("User_Login_Details", PRIVATE_MODE);
    }

    public SessionManager(){

    }

    public static SessionManager getInstance(Context context) {
        if (SessionManager.m_sessMgr != null) return SessionManager.m_sessMgr;
        SessionManager.m_sessMgr = new SessionManager(context);
        return SessionManager.m_sessMgr;
    }

    public boolean isLoggedIn() {
        return this.m_pref.getBoolean("isActive", false);
    }

    public SharedPreferences getUserDetails() {
        return this.m_pref;
    }

    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this.m_context);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        int MY_SOCKET_TIMEOUT_MS = 5000;
        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}