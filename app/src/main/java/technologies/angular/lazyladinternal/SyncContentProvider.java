package technologies.angular.lazyladinternal;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * Created by Saurabh on 11/02/15.
 */
public class SyncContentProvider extends ContentProvider {
    public static final String AUTHORITY = "technologies.angular.lazyladinternal.provider";
    public static Uri CONTENT_URI_SQL;
    public static Uri CONTENT_URI_INSERT;
    public static Uri CONTENT_URI_UPDATE;
    public static Uri CONTENT_URI_SELECT;
    public static Uri CONTENT_URI_DELETE;
    public static Uri CONTENT_URI_UPDATENSELECT;

    private static UriMatcher uriMatcher;
    private DBHelper m_dbHelper;
    private SQLiteDatabase m_db;
    private Cursor m_cursor;

    static{
        SyncContentProvider.CONTENT_URI_SQL = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SQL"));
        SyncContentProvider.CONTENT_URI_INSERT = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/INSERT"));
        SyncContentProvider.CONTENT_URI_UPDATE = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATE"));
        SyncContentProvider.CONTENT_URI_SELECT = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/SELECT"));
        SyncContentProvider.CONTENT_URI_DELETE = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/DELETE"));
        SyncContentProvider.CONTENT_URI_UPDATENSELECT = Uri.parse((String)("content://technologies.angular.lazyladinternal.provider/UPDATENSELECT"));
        SyncContentProvider.uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SQL", 1);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SQL/#", 2);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "INSERT", 3);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "INSERT/#", 4);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "UPDATE", 5);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "UPDATE/#", 6);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SELECT", 7);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "SELECT/#", 8);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "DELETE", 9);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "DELETE/#", 10);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "UPDATENSELECT", 11);
        SyncContentProvider.uriMatcher.addURI("technologies.angular.lazyladinternal.provider", "UPDATENSELECT/#", 12);

    }

    @Override
    public boolean onCreate() {
        Log.i("Step1", "Content Provider");
        m_dbHelper=new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String query_stmt,
                        String[] selectionArgs, String sortOrder) {
        Log.i("Step2", "In Queery Function");
        switch(SyncContentProvider.uriMatcher.match(uri)){
            default: {
                return this.m_cursor;
            }
            case 3: {
                this.insertQ(uri, query_stmt);
                return this.m_cursor;
            }
            case 5: {
                this.updateQ(uri, query_stmt);
                return this.m_cursor;
            }
            case 7: {
                Log.i("Hi In Content Provider", "It is working ");
                this.m_cursor=this.selectQ(uri, query_stmt);
                Log.i("Got Cursor", "Everything fine here");
                return this.m_cursor;
            }
            case 9: {
                this.deleteQ(uri, query_stmt);
                return this.m_cursor;
            }
        }
        //return this.m_cursor;
    }

    public Cursor selectQ(Uri uri, String query_stmt){
        Log.i("Step 2", "It is working ");
        Cursor cursor=null;
        m_db=m_dbHelper.getReadableDatabase();
        cursor=this.m_db.rawQuery(query_stmt, (String[])(null));
        Log.i("Step 3", "Query Run");
        //m_db.close();
        Log.i("Closing is good", "No issues here");
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    public int insertQ(Uri uri, String query_stmt){
        m_db=m_dbHelper.getWritableDatabase();
        this.m_db.execSQL(query_stmt);
        //m_db.close();
        return 0;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public int deleteQ(Uri uri, String query_stmt) {
        m_db=m_dbHelper.getWritableDatabase();
        this.m_db.execSQL(query_stmt);
        //m_db.close();
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return 0;
    }

    public int updateQ(Uri uri, String query_stmt) {
        Log.i("SyncContentProvider", "IN Update");
        m_db=m_dbHelper.getWritableDatabase();
        this.m_db.execSQL(query_stmt);
        Log.i("SyncContentProvider", "Update Complete");
        //m_db.close();
        return 0;
    }
}
